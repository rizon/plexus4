/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2014-2016 plexus development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "stdinc.h"
#include "list.h"
#include "hook.h"

struct hook iline_attach_hook =
{
  .name = "iline_attach"
};

struct hook can_send_hook =
{
  .name = "can_send"
};

struct hook local_user_register_hook =
{
  .name = "local_user_register"
};

struct hook pre_server_burst_hook =
{
  .name = "pre_server_burst"
};

void hook_add(struct Event *ev)
{
  dlinkAdd(ev, &ev->node, &ev->event->listeners);
}

void hook_del(struct Event *ev)
{
  dlinkDelete(&ev->node, &ev->event->listeners);
}

void hook_execute(struct hook *h, void *arg)
{
  dlink_node *node;

  DLINK_FOREACH(node, h->listeners.head)
  {
    struct Event *e = node->data;
    e->handler(arg);
  }
}

