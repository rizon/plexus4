/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 1997-2014 ircd-hybrid development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

/*! \file s_auth.c
 * \brief Functions for querying a users ident.
 * \version $Id$
 */

/*
 * Changes:
 *   July 6, 1999 - Rewrote most of the code here. When a client connects
 *     to the server and passes initial socket validation checks, it
 *     is owned by this module (auth) which returns it to the rest of the
 *     server when dns and auth queries are finished. Until the client is
 *     released, the server does not know it exists and does not process
 *     any messages from it.
 *     --Bleep  Thomas Helvey <tomh@inxpress.net>
 */

#include "stdinc.h"
#include "list.h"
#include "ircd_defs.h"
#include "fdlist.h"
#include "s_auth.h"
#include "conf.h"
#include "client.h"
#include "event.h"
#include "irc_string.h"
#include "ircd.h"
#include "packet.h"
#include "irc_res.h"
#include "s_bsd.h"
#include "log.h"
#include "send.h"
#include "mempool.h"
#include "dnsbl.h"


static const char *HeaderMessages[] =
{
  ":%s NOTICE * :*** Looking up your hostname...",
  ":%s NOTICE * :*** Found your hostname",
  ":%s NOTICE * :*** Couldn't look up your hostname",
  ":%s NOTICE * :*** Found your hostname (cached)",
  ":%s NOTICE * :*** Couldn't look up your hostname (cached)",
  ":%s NOTICE * :*** Checking Ident",
  ":%s NOTICE * :*** Got Ident response",
  ":%s NOTICE * :*** No Ident response",
  ":%s NOTICE * :*** Your forward and reverse DNS do not match, ignoring hostname.",
  ":%s NOTICE * :*** Your hostname is too long, ignoring hostname"
};

enum
{
  REPORT_DO_DNS,
  REPORT_FIN_DNS,
  REPORT_FAIL_DNS,
  REPORT_FIN_DNS_CACHED,
  REPORT_FAIL_DNS_CACHED,
  REPORT_DO_ID,
  REPORT_FIN_ID,
  REPORT_FAIL_ID,
  REPORT_IP_MISMATCH,
  REPORT_HOST_TOOLONG
};

#define sendheader(c, i) sendto_one((c), HeaderMessages[(i)], me.name)

static dlink_list auth_doing_list = { NULL, NULL, 0 };

static EVH timeout_auth_queries_event;

static PF read_auth_reply;
static CNCB auth_connect_callback;

/* auth_init
 *
 * Initialise the auth code
 */
void
auth_init(void)
{
  eventAddIsh("timeout_auth_queries_event", timeout_auth_queries_event, NULL, 1);
}

/*
 * make_auth_request - allocate a new auth request
 */
static struct AuthRequest *
make_auth_request(struct Client *client)
{
  struct AuthRequest *request = &client->localClient->auth;

  memset(request, 0, sizeof(*request));

  request->client  = client;
  request->timeout = CurrentTime + GlobalSetOptions.ident_timeout;

  return request;
}

/*
 * release_auth_client - release auth client from auth system
 * this adds the client into the local client lists so it can be read by
 * the main io processing loop
 */
void
release_auth_client(struct AuthRequest *auth)
{
  struct Client *client = auth->client;

  if (IsInCrit(auth) || IsDoingAuth(auth) || IsDNSPending(auth))
    return;

  if (IsInAuth(auth))
  {
    dlinkDelete(&auth->node, &auth_doing_list);
    ClearInAuth(auth);
  }

  dlinkAdd(client, &client->node, &global_client_list);

  client->localClient->since     = CurrentTime;
  client->localClient->lasttime  = CurrentTime;
  client->localClient->firsttime = CurrentTime;
  client->flags |= FLAGS_FINISHED_AUTH;

  comm_setselect(&client->localClient->fd, COMM_SELECT_READ, read_packet, client);
}

/*
 * auth_dns_callback - called when resolver query finishes
 * if the query resulted in a successful search, name will contain
 * a non-NULL pointer, otherwise name will be NULL.
 * set the client on it's way to a connection completion, regardless
 * of success of failure
 */
static void
auth_dns_callback(void *vptr, const struct irc_ssaddr *addr, const char *name)
{
  struct AuthRequest *auth = vptr;
  struct ip_entry *ip = find_or_add_ip(&auth->client->localClient->ip);

  ip->host_invalid = 1;
  *ip->host = '\0';

  ClearDNSPending(auth);

  if (name != NULL)
  {
    const struct sockaddr_in *v4, *v4dns;
#ifdef IPV6
    const struct sockaddr_in6 *v6, *v6dns;
#endif
    int good = 1;

#ifdef IPV6
    if (auth->client->localClient->ip.ss.ss_family == AF_INET6)
    {
      v6 = (const struct sockaddr_in6 *)&auth->client->localClient->ip;
      v6dns = (const struct sockaddr_in6 *)addr;
      if (memcmp(&v6->sin6_addr, &v6dns->sin6_addr, sizeof(struct in6_addr)) != 0)
      {
        sendheader(auth->client, REPORT_IP_MISMATCH);
        good = 0;
      }
    }
    else
#endif
    {
      v4 = (const struct sockaddr_in *)&auth->client->localClient->ip;
      v4dns = (const struct sockaddr_in *)addr;
      if(v4->sin_addr.s_addr != v4dns->sin_addr.s_addr)
      {
        sendheader(auth->client, REPORT_IP_MISMATCH);
        good = 0;
      }
    }
    if (good && strlen(name) <= HOSTLEN)
    {
      strlcpy(auth->client->host, name, sizeof(auth->client->host));
      strlcpy(auth->client->realhost, name, sizeof(auth->client->realhost));
      ip->host_invalid = 0;
      strlcpy(ip->host, name, sizeof(ip->host));
      sendheader(auth->client, REPORT_FIN_DNS);
    }
    else if (strlen(name) > HOSTLEN)
      sendheader(auth->client, REPORT_HOST_TOOLONG);
  }
  else
    sendheader(auth->client, REPORT_FAIL_DNS);

  release_auth_client(auth);
}

/*
 * authsenderr - handle auth send errors
 */
static void
auth_error(struct AuthRequest *auth)
{
  ++ServerStats.is_abad;

  fd_close(&auth->fd);

  ClearAuth(auth);

  sendheader(auth->client, REPORT_FAIL_ID);

  release_auth_client(auth);
}

/*
 * start_auth_query - Flag the client to show that an attempt to
 * contact the ident server on
 * the client's host.  The connect and subsequently the socket are all put
 * into 'non-blocking' mode.  Should the connect or any later phase of the
 * identifing process fail, it is aborted and the user is given a username
 * of "unknown".
 */
static int
start_auth_query(struct AuthRequest *auth)
{
  struct irc_ssaddr localaddr;
#ifdef IPV6
  struct sockaddr_in6 *v6;
#else
  struct sockaddr_in *v4;
#endif

  /* open a socket of the same type as the client socket */
  if (comm_open(&auth->fd, auth->client->localClient->ip.ss.ss_family,
                SOCK_STREAM, 0, "ident") == -1)
  {
    report_error(L_ALL, "creating auth stream socket %s:%s",
                 get_client_name(auth->client, SHOW_IP), errno);
    ilog(LOG_TYPE_IRCD, "Unable to create auth socket for %s",
        get_client_name(auth->client, SHOW_IP));
    ++ServerStats.is_abad;
    ClearAuth(auth);
    return 0;
  }

  sendheader(auth->client, REPORT_DO_ID);

  memcpy(&localaddr, &auth->client->localClient->localaddr, sizeof(struct irc_ssaddr));
#ifdef IPV6
  v6 = (struct sockaddr_in6 *) &localaddr;
  v6->sin6_port = htons(0);
#else
  v4 = (struct sockaddr_in *) &localaddr;
  v4->sin_port = htons(0);
#endif
  fix_irc_ssaddr(&localaddr);

  /* The auth system has its own timeout check, using an ordered queue,
   * so a timeout here is not necessary
   */
  comm_connect_tcp(&auth->fd, auth->client->sockhost, 113,
      (struct sockaddr *) &localaddr, localaddr.ss_len,
      auth_connect_callback, auth, auth->client->localClient->ip.ss.ss_family, 0);
  return 1; /* We suceed here for now */
}

/*
 * GetValidIdent - parse ident query reply from identd server
 *
 * Inputs        - pointer to ident buf
 * Output        - NULL if no valid ident found, otherwise pointer to name
 * Side effects  -
 */
/*
 * A few questions have been asked about this mess, obviously
 * it should have been commented better the first time.
 * The original idea was to remove all references to libc from ircd-hybrid.
 * Instead of having to write a replacement for sscanf(), I did a
 * rather gruseome parser here so we could remove this function call.
 * Note, that I had also removed a few floating point printfs as well (though
 * now we are still stuck with a few...)
 * Remember, we have a replacement ircd sprintf, we have bleeps fputs lib
 * it would have been nice to remove some unneeded code.
 * Oh well. If we don't remove libc stuff totally, then it would be
 * far cleaner to use sscanf()
 *
 * - Dianora
 */
static char *
GetValidIdent(char *buf)
{
  int   remp = 0;
  int   locp = 0;
  char* colon1Ptr;
  char* colon2Ptr;
  char* colon3Ptr;
  char* commaPtr;
  char* remotePortString;

  /* All this to get rid of a sscanf() fun. */
  remotePortString = buf;

  if ((colon1Ptr = strchr(remotePortString,':')) == NULL)
    return 0;
  *colon1Ptr = '\0';
  colon1Ptr++;

  if ((colon2Ptr = strchr(colon1Ptr,':')) == NULL)
    return 0;
  *colon2Ptr = '\0';
  colon2Ptr++;

  if ((commaPtr = strchr(remotePortString, ',')) == NULL)
    return 0;
  *commaPtr = '\0';
  commaPtr++;

  if ((remp = atoi(remotePortString)) == 0)
    return 0;

  if ((locp = atoi(commaPtr)) == 0)
    return 0;

  /* look for USERID bordered by first pair of colons */
  if (strstr(colon1Ptr, "USERID") == NULL)
    return 0;

  if ((colon3Ptr = strchr(colon2Ptr,':')) == NULL)
    return 0;
  *colon3Ptr = '\0';
  colon3Ptr++;
  return (colon3Ptr);
}

/*
 * start_auth
 *
 * inputs	- pointer to client to auth
 * output	- NONE
 * side effects	- starts auth (identd) and dns queries for a client
 */
void
start_auth(struct Client *client)
{
  struct AuthRequest *auth = NULL;
  struct ip_entry *ip = find_or_add_ip(&client->localClient->ip);

  assert(client != NULL);

  auth = make_auth_request(client);

  SetInAuth(auth);
  dlinkAddTail(auth, &auth->node, &auth_doing_list);

  SetInCrit(auth);

  if (ip->host_invalid)
  {
    sendheader(client, REPORT_FAIL_DNS_CACHED);
    ++ServerStats.is_dc;
  }
  else if (!*ip->host)
  {
    sendheader(client, REPORT_DO_DNS);

    SetDNSPending(auth);
    gethost_byaddr(&auth->query, auth_dns_callback, auth, &client->localClient->ip);
  }
  else
  {
    strlcpy(client->host, ip->host, sizeof(client->host));
    strlcpy(client->realhost, ip->host, sizeof(client->realhost));
    sendheader(client, REPORT_FIN_DNS_CACHED);
    ++ServerStats.is_dc;
  }

  if (ConfigFileEntry.disable_auth == 0)
  {
    SetDoingAuth(auth);
    start_auth_query(auth);
  }

  start_dnsbl_lookup(client);

  ClearInCrit(auth);
  release_auth_client(auth);
}

/*
 * timeout_auth_queries - timeout resolver and identd requests
 * allow clients through if requests failed
 */
static void
timeout_auth_queries_event(void *notused)
{
  dlink_node *ptr = NULL, *next_ptr = NULL;

  DLINK_FOREACH_SAFE(ptr, next_ptr, auth_doing_list.head)
  {
    struct AuthRequest *auth = ptr->data;

    if (auth->timeout > CurrentTime)
      break;

    if (IsDoingAuth(auth))
    {
      ++ServerStats.is_abad;
      fd_close(&auth->fd);
      ClearAuth(auth);
      sendheader(auth->client, REPORT_FAIL_ID);
    }

    if (IsDNSPending(auth))
    {
      struct ip_entry *ip = find_or_add_ip(&auth->client->localClient->ip);
      ip->host_invalid = 1;
      *ip->host = '\0';

      delete_resolver_query(&auth->query);
      ClearDNSPending(auth);
      sendheader(auth->client, REPORT_FAIL_DNS);
    }

    ilog(LOG_TYPE_DEBUG, "DNS/AUTH timeout %s",
         get_client_name(auth->client, SHOW_IP));
    release_auth_client(auth);
  }
}

/*
 * auth_connect_callback() - deal with the result of comm_connect_tcp()
 *
 * If the connection failed, we simply close the auth fd and report
 * a failure. If the connection suceeded send the ident server a query
 * giving "theirport , ourport". The write is only attempted *once* so
 * it is deemed to be a fail if the entire write doesn't write all the
 * data given.  This shouldnt be a problem since the socket should have
 * a write buffer far greater than this message to store it in should
 * problems arise. -avalon
 */
static void
auth_connect_callback(fde_t *fd, int error, void *data)
{
  struct AuthRequest *auth = data;
  char authbuf[32];
  uint16_t uport, tport;
#ifdef IPV6
  struct sockaddr_in6 *v6;
#else
  struct sockaddr_in *v4;
#endif
  int len;

  if (error != COMM_OK)
  {
    auth_error(auth);
    return;
  }

#ifdef IPV6
  v6 = (struct sockaddr_in6 *) &auth->client->localClient->localaddr;
  uport = ntohs(v6->sin6_port);
  v6 = (struct sockaddr_in6 *) &auth->client->localClient->ip;
  tport = ntohs(v6->sin6_port);
#else
  v4 = (struct sockaddr_in *) &auth->client->localClient->localaddr;
  uport = ntohs(v4->sin_port);
  v4 = (struct sockaddr_in *) &auth->client->localClient->ip;
  tport = ntohs(v4->sin_port);
#endif

  len = snprintf(authbuf, sizeof(authbuf), "%u , %u\r\n", tport, uport);

  if (send(fd->fd, authbuf, len, 0) != len)
  {
    auth_error(auth);
    return;
  }

  read_auth_reply(&auth->fd, auth);
}

/*
 * read_auth_reply - read the reply (if any) from the ident server
 * we connected to.
 * We only give it one shot, if the reply isn't good the first time
 * fail the authentication entirely. --Bleep
 */
#define AUTH_BUFSIZ 128

static void
read_auth_reply(fde_t *fd, void *data)
{
  struct AuthRequest *auth = data;
  char *s = NULL;
  char *t = NULL;
  int len;
  int count;
  char buf[AUTH_BUFSIZ + 1]; /* buffer to read auth reply into */

  /* Why?
   * Well, recv() on many POSIX systems is a per-packet operation,
   * and we do not necessarily want this, because on lowspec machines,
   * the ident response may come back fragmented, thus resulting in an
   * invalid ident response, even if the ident response was really OK.
   *
   * So PLEASE do not change this code to recv without being aware of the
   * consequences.
   *
   *    --nenolod
   */
  len = read(fd->fd, buf, AUTH_BUFSIZ);

  if (len < 0)
  {
    if (ignoreErrno(errno))
      comm_setselect(fd, COMM_SELECT_READ, read_auth_reply, auth);
    else
      auth_error(auth);
    return;
  }

  if (len > 0)
  {
    buf[len] = '\0';

    if ((s = GetValidIdent(buf)))
    {
      t = auth->client->username;

      while (*s == '~' || *s == '^')
        s++;

      for (count = USERLEN; *s && count; s++)
      {
        if (*s == '@')
          break;
        if (!IsSpace(*s) && *s != ':' && *s != '[')
        {
          *t++ = *s;
          count--;
        }
      }

      *t = '\0';
    }
  }

  fd_close(fd);

  ClearAuth(auth);

  if (s == NULL)
  {
    sendheader(auth->client, REPORT_FAIL_ID);
    ++ServerStats.is_abad;
  }
  else
  {
    sendheader(auth->client, REPORT_FIN_ID);
    ++ServerStats.is_asuc;
    SetGotId(auth->client);
  }

  release_auth_client(auth);
}

/*
 * delete_auth()
 */
void
delete_auth(struct AuthRequest *auth)
{
  if (IsDNSPending(auth))
    delete_resolver_query(&auth->query);

  if (IsDoingAuth(auth))
    fd_close(&auth->fd);

  if (IsInAuth(auth))
  {
    dlinkDelete(&auth->node, &auth_doing_list);
    ClearInAuth(auth);
  }
}
