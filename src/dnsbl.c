/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2013-2016 plexus development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "client.h"
#include "dnsbl.h"
#include "conf.h"
#include "hostmask.h"
#include "s_misc.h"
#include "ircd.h"
#include "send.h"
#include "list.h"
#include "irc_res.h"
#include "s_user.h"
#include "irc_string.h"
#include "memory.h"
#include "log.h"
#include "numeric.h"

static int
dnsbl_matches(struct dnsbl_entry *entry, const struct irc_ssaddr *addr)
{
  dlink_node *ptr;

  if (addr == NULL || ((const struct sockaddr *) addr)->sa_family != AF_INET)
    return 0;

  DLINK_FOREACH(ptr, entry->matches.head)
  {
    struct dnsbl_match *match = ptr->data;

    if (match_ipv4(addr, &match->ip, match->bits))
    {
      return 1;
    }
  }

  return 0;
}

static void
dnsbl_callback(struct DnsblLookup *lookup, const struct irc_ssaddr *addr, const char *name)
{
  struct Client *cptr = lookup->cptr;
  struct dnsbl_entry *entry = lookup->entry;
  struct ip_entry *ip = find_or_add_ip(&cptr->localClient->ip);
  int hit = 0;

  if (dnsbl_matches(entry, addr))
  {
    const struct sockaddr_in *in = (const struct sockaddr_in *) addr;
    uint8_t result = in->sin_addr.s_addr >> 24;

    ++entry->hits;

    /* Add the result to ipcache */
    ip->dnsbl_entry = entry;
    ip->dnsbl_result = result;
    ip->dnsbl_clear = 0;
    /* make sure cache entry sticks around */
    ip->dnsbl_time = CurrentTime;

    hit = 1;
  }

  /* remove and free lookup */
  assert(dlinkFind(&cptr->localClient->dnsbl_queries, lookup));
  dlinkDelete(&lookup->node, &cptr->localClient->dnsbl_queries);

  assert(dlinkFind(&entry->lookups, lookup));
  dlinkDelete(&lookup->enode, &entry->lookups);

  MyFree(lookup);

  if (hit)
  {
    /* now that they're listed in something we can drop further queries as we don't need them.
     * This can't be called if we're iterating the query list like in timeout_query_list */
    clear_dnsbl_lookup(cptr);
  }
  else if (!dlink_list_length(&cptr->localClient->dnsbl_queries))
  {
    /* this was the last query, mark ip as clear in ipcache */
    ip->dnsbl_clear = 1;
  }

  if (IsUnknown(cptr))
  {
    /* maybe they can register now? */
    if (!cptr->localClient->registration)
      register_local_user(cptr);
  }
  else if (hit)
  {
    /* ban if this is my client */
    dnsbl_ban(cptr, ip);
  }
}


/*
 * start_dnsbl_lookup
 *
 * inputs       - struct Client *
 * side effects - The client is checked for being on the DNS blacklists.
 */
void
start_dnsbl_lookup(struct Client *cptr)
{
  dlink_node *ptr;
  struct ip_entry *ipc;
  char ipbuf[RFC1035_MAX_DOMAIN_LENGTH + 1];

  assert(MyConnect(cptr));

  if (dlink_list_length(&dnsbl_items) == 0)
    return;

  ipc = find_or_add_ip(&cptr->localClient->ip);

  /* it's possible to race and have both dnsbl_entry and dnsbl_clear. prioritize dnsbl_entry */
  if (ipc->dnsbl_entry != NULL)
  {
    /* cache hit */
    ++ServerStats.is_rblh;
    ipc->dnsbl_cached = 1;
    ipc->dnsbl_time = CurrentTime; /* make sure ipcache entry lives long enough to ban the user on registration */
    /* ban if this is my client */
    dnsbl_ban(cptr, ipc);
    return;
  }

  if (ipc->dnsbl_clear)
  {
    /* cache miss */
    ++ServerStats.is_rblc;
    return;
  }

  if (cptr->localClient->ip.ss.ss_family == AF_INET)
  {
    uint8_t *ip = (uint8_t *) &((struct sockaddr_in *) &cptr->localClient->ip)->sin_addr.s_addr;
    snprintf(ipbuf, sizeof(ipbuf), "%u.%u.%u.%u", (unsigned int) ip[3], (unsigned int) ip[2], (unsigned int) ip[1], (unsigned int) ip[0]);
  }
  else if (cptr->localClient->ip.ss.ss_family == AF_INET6)
  {
    char *ip = (char *) ((struct sockaddr_in6 *) &cptr->localClient->ip)->sin6_addr.s6_addr;
    char dest[33]; /* 16*2 + 1 */
    char *bufptr = ipbuf;

    base16_encode(dest, sizeof(dest), ip, 16);

    /* skip trailing \0 */
    for (int i = sizeof(dest) - 2; i >= 0; --i)
    {
      *bufptr++ = dest[i];
      *bufptr++ = '.';
    }

    *--bufptr = 0; /* remove trailing . */
  }
  else
    return;

  DLINK_FOREACH(ptr, dnsbl_items.head)
  {
    struct dnsbl_entry *dentry = ptr->data;
    char hostbuf[RFC1035_MAX_DOMAIN_LENGTH + 1];
    struct DnsblLookup *info;

    info = MyMalloc(sizeof(struct DnsblLookup));
    dlinkAdd(info, &info->node, &cptr->localClient->dnsbl_queries);
    dlinkAdd(info, &info->enode, &dentry->lookups);

    snprintf(hostbuf, sizeof(hostbuf), "%s.%s", ipbuf, dentry->name);

    info->cptr = cptr;
    info->entry = dentry;

    gethost_byname_type(&info->query, (dns_callback_fnc) dnsbl_callback, info, hostbuf, T_A);
  }
}

static void
cancel_lookup(struct DnsblLookup *info)
{
    delete_resolver_query(&info->query);

    assert(dlinkFind(&info->cptr->localClient->dnsbl_queries, info));
    dlinkDelete(&info->node, &info->cptr->localClient->dnsbl_queries);

    assert(dlinkFind(&info->entry->lookups, info));
    dlinkDelete(&info->enode, &info->entry->lookups);

    MyFree(info);
}

/*
 * clear_dnsbl_lookup
 *
 * inputs       - struct Client *
 * side effects - Pending DNS queries for this client will be canceled
 */
void
clear_dnsbl_lookup(struct Client *cptr)
{
  dlink_node *ptr, *next_ptr;

  assert(MyConnect(cptr));

  DLINK_FOREACH_SAFE(ptr, next_ptr, cptr->localClient->dnsbl_queries.head)
  {
    struct DnsblLookup *info = ptr->data;
    cancel_lookup(info);
  }
}

void
cancel_blacklist(struct dnsbl_entry *bl)
{
  dlink_node *ptr, *next_ptr;

  /* cancel all associated lookups */
  DLINK_FOREACH_SAFE(ptr, next_ptr, bl->lookups.head)
  {
    struct DnsblLookup *info = ptr->data;
    cancel_lookup(info);
  }

  /* remove blacklist from ipcache */
  ipcache_cleanup_dnsbl(bl);
}

const char *
dnsbl_format_reason(struct Client *source_p, struct ip_entry *ip)
{
  static char buf[IRCD_BUFSIZE];
  char codebuf[4];

  strlcpy(buf, ip->dnsbl_entry->reason, sizeof(buf));

  str_replace(buf, buf, sizeof(buf), "%i", source_p->sockhost);
  str_replace(buf, buf, sizeof(buf), "%d", smalldate(CurrentTime, 0));
  snprintf(codebuf, sizeof(codebuf), "%u", ip->dnsbl_result);
  str_replace(buf, buf, sizeof(buf), "%c", codebuf);

  return buf;
}

void
dnsbl_ban(struct Client *source_p, struct ip_entry *ip)
{
  const char *user_reason, *channel_reason;

  /* unknown clients are rejected later at registration */
  if (!MyClient(source_p) || IsExemptDnsbl(source_p))
    return;

  user_reason = dnsbl_format_reason(source_p, ip);
  channel_reason = !EmptyString(ConfigFileEntry.kline_reason) ? ConfigFileEntry.kline_reason : user_reason;

  sendto_snomask(SNO_REJ, L_ALL,
                 "Exiting %s, listed in DNSBL %s with result %d%s",
                 get_client_name(source_p, SHOW_IP), ip->dnsbl_entry->name, ip->dnsbl_result, ip->dnsbl_cached ? " (cached)" : "");

  sendto_one(source_p, form_str(ERR_YOUREBANNEDCREEP),
             me.name, source_p->name, user_reason);

  exit_client(source_p, &me, channel_reason);
}

