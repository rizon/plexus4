/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 1997-2014 ircd-hybrid development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

/*! \file numeric.c
 * \brief Declarations of numeric replies.
 * \version $Id$
 */

#include "stdinc.h"
#include "numeric.h"


static const char *const replies[] =
{
  /* 001 */  [RPL_WELCOME]              = ":%s 001 %s :Welcome to the %s Internet Relay Chat Network %s",
  /* 002 */  [RPL_YOURHOST]             = ":%s 002 %s :Your host is %s, running version %s",
  /* 003 */  [RPL_CREATED]              = ":%s 003 %s :This server was created %s",
  /* 004 */  [RPL_MYINFO]               = ":%s 004 %s %s %s %s %s",
  /* 005 */  [RPL_ISUPPORT]             = ":%s 005 %s %s :are supported by this server",
  /* 008 */  [RPL_SNOMASK]              = ":%s 008 %s %s :Server notice mask",
  /* 010 */  [RPL_REDIR]                = ":%s 010 %s %s %u :Please use this Server/Port instead",
  /* 015 */  [RPL_MAP]                  = ":%s 015 %s :%s%s",
  /* 016 */  [RPL_MAPMORE]              = ":%s 016 %s :%s%s --> *more*",
  /* 017 */  [RPL_MAPEND]               = ":%s 017 %s :End of /MAP",
  /* 200 */  [RPL_TRACELINK]            = ":%s 200 %s Link %s %s %s",
  /* 201 */  [RPL_TRACECONNECTING]      = ":%s 201 %s Try. %s %s",
  /* 202 */  [RPL_TRACEHANDSHAKE]       = ":%s 202 %s H.S. %s %s",
  /* 203 */  [RPL_TRACEUNKNOWN]         = ":%s 203 %s ???? %s %s (%s) %d",
  /* 204 */  [RPL_TRACEOPERATOR]        = ":%s 204 %s Oper %s %s (%s) %lu %u",
  /* 205 */  [RPL_TRACEUSER]            = ":%s 205 %s User %s %s (%s) %lu %u",
  /* 206 */  [RPL_TRACESERVER]          = ":%s 206 %s Serv %s %uS %uC %s %s!%s@%s %lu",
  /* 207 */  [RPL_TRACECAPTURED]        = ":%s 207 %s Capt %s %s (%s) %lu %lu",
  /* 208 */  [RPL_TRACENEWTYPE]         = ":%s 208 %s <newtype> 0 %s",
  /* 209 */  [RPL_TRACECLASS]           = ":%s 209 %s Class %s %d",
  /* 211 */  [RPL_STATSLINKINFO]        = ":%s 211 %s %s %u %u %llu %u %llu :%u %u %s",
  /* 212 */  [RPL_STATSCOMMANDS]        = ":%s 212 %s %s %u %llu :%u",
  /* 213 */  [RPL_STATSCLINE]           = ":%s 213 %s %c %s %s %s %d %s",
  /* 214 */  [RPL_STATSNLINE]           = ":%s 214 %s %c %s * %s %d %s",
  /* 215 */  [RPL_STATSILINE]           = ":%s 215 %s %c %s * %s@%s %d %s",
  /* 216 */  [RPL_STATSKLINE]           = ":%s 216 %s %c %s * %s :%s",
  /* 217 */  [RPL_STATSQLINE]           = ":%s 217 %s %c %u %s :%s",
  /* 218 */  [RPL_STATSYLINE]           = ":%s 218 %s %c %s %u %u %u %u %u %u %u/%u %u/%u %s %u %u",
  /* 219 */  [RPL_ENDOFSTATS]           = ":%s 219 %s %c :End of /STATS report",
  /* 220 */  [RPL_STATSPLINE]           = ":%s 220 %s %c %d %s %s %d %s :%s",
  /* 221 */  [RPL_UMODEIS]              = ":%s 221 %s %s",
  /* 225 */  [RPL_STATSDLINE]           = ":%s 225 %s %c %s :%s",
  /* 226 */  [RPL_STATSALINE]           = ":%s 226 %s :%s %d",
  /* 227 */  [RPL_STATSBLINE]           = ":%s 227 %s %c %s %lu",
  /* 241 */  [RPL_STATSLLINE]           = ":%s 241 %s %c %s * %s %d %s",
  /* 242 */  [RPL_STATSUPTIME]          = ":%s 242 %s :Server Up %d days, %d:%02d:%02d",
  /* 243 */  [RPL_STATSOLINE]           = ":%s 243 %s %c %s@%s * %s %s %s",
  /* 244 */  [RPL_STATSHLINE]           = ":%s 244 %s %c %s * %s %d %s",
  /* 245 */  [RPL_STATSTLINE]           = ":%s 245 %s T %s %s",
  /* 246 */  [RPL_STATSSERVICE]         = ":%s 246 %s %c %s * %s %d %d",
  /* 247 */  [RPL_STATSXLINE]           = ":%s 247 %s %c %d %s :%s",
  /* 248 */  [RPL_STATSULINE]           = ":%s 248 %s U %s %s@%s %s",
  /* 250 */  [RPL_STATSCONN]            = ":%s 250 %s :Highest connection count: %d (%d clients) (%llu connections received)",
  /* 251 */  [RPL_LUSERCLIENT]          = ":%s 251 %s :There are %d users and %d invisible on %d servers",
  /* 252 */  [RPL_LUSEROP]              = ":%s 252 %s %d :IRC Operators online",
  /* 253 */  [RPL_LUSERUNKNOWN]         = ":%s 253 %s %d :unknown connection(s)",
  /* 254 */  [RPL_LUSERCHANNELS]        = ":%s 254 %s %d :channels formed",
  /* 255 */  [RPL_LUSERME]              = ":%s 255 %s :I have %d clients and %d servers",
  /* 256 */  [RPL_ADMINME]              = ":%s 256 %s :Administrative info about %s",
  /* 257 */  [RPL_ADMINLOC1]            = ":%s 257 %s :%s",
  /* 258 */  [RPL_ADMINLOC2]            = ":%s 258 %s :%s",
  /* 259 */  [RPL_ADMINEMAIL]           = ":%s 259 %s :%s",
  /* 262 */  [RPL_ENDOFTRACE]           = ":%s 262 %s %s :End of TRACE",
  /* 263 */  [RPL_LOAD2HI]              = ":%s 263 %s :Server load is temporarily too heavy. Please wait a while and try again.",
  /* 265 */  [RPL_LOCALUSERS]           = ":%s 265 %s :Current local users: %d  Max: %d",
  /* 266 */  [RPL_GLOBALUSERS]          = ":%s 266 %s :Current global users: %d  Max: %d",
  /* 276 */  [RPL_WHOISCERTFP]          = ":%s 276 %s %s :is using a client certificate",
  /* 281 */  [RPL_ACCEPTLIST]           = ":%s 281 %s :%s",
  /* 282 */  [RPL_ENDOFACCEPT]          = ":%s 282 %s :End of /ACCEPT list.",
  /* 301 */  [RPL_AWAY]                 = ":%s 301 %s %s :%s",
  /* 302 */  [RPL_USERHOST]             = ":%s 302 %s :%s",
  /* 303 */  [RPL_ISON]                 = ":%s 303 %s :",
  /* 305 */  [RPL_UNAWAY]               = ":%s 305 %s :You are no longer marked as being away",
  /* 306 */  [RPL_NOWAWAY]              = ":%s 306 %s :You have been marked as being away",
  /* 307 */  [RPL_WHOISREGNICK]         = ":%s 307 %s %s :has identified for %s",
  /* 311 */  [RPL_WHOISUSER]            = ":%s 311 %s %s %s %s * :%s",
  /* 312 */  [RPL_WHOISSERVER]          = ":%s 312 %s %s %s :%s",
  /* 313 */  [RPL_WHOISOPERATOR]        = ":%s 313 %s %s :%s",
  /* 314 */  [RPL_WHOWASUSER]           = ":%s 314 %s %s %s %s * :%s",
  /* 315 */  [RPL_ENDOFWHO]             = ":%s 315 %s %s :End of /WHO list.",
  /* 317 */  [RPL_WHOISIDLE]            = ":%s 317 %s %s %u %d :seconds idle, signon time",
  /* 318 */  [RPL_ENDOFWHOIS]           = ":%s 318 %s %s :End of /WHOIS list.",
  /* 319 */  [RPL_WHOISCHANNELS]        = ":%s 319 %s %s :%s",
  /* 321 */  [RPL_LISTSTART]            = ":%s 321 %s Channel :Users  Name",
  /* 322 */  [RPL_LIST]                 = ":%s 322 %s %s %d :%s%s",
  /* 323 */  [RPL_LISTEND]              = ":%s 323 %s :End of /LIST",
  /* 324 */  [RPL_CHANNELMODEIS]        = ":%s 324 %s %s %s %s",
  /* 329 */  [RPL_CREATIONTIME]         = ":%s 329 %s %s %lu",
  /* 330 */  [RPL_WHOISACCOUNT]         = ":%s 330 %s %s %s :is logged in as",
  /* 331 */  [RPL_NOTOPIC]              = ":%s 331 %s %s :No topic is set.",
  /* 332 */  [RPL_TOPIC]                = ":%s 332 %s %s :%s",
  /* 333 */  [RPL_TOPICWHOTIME]         = ":%s 333 %s %s %s %lu",
  /* 337 */  [RPL_WHOISTEXT]            = ":%s 337 %s %s :%s",
  /* 338 */  [RPL_WHOISACTUALLY]        = ":%s 338 %s %s :%s %s",
  /* 341 */  [RPL_INVITING]             = ":%s 341 %s %s %s",
  /* 346 */  [RPL_INVITELIST]            = ":%s 346 %s %s %s %s %lu",
  /* 347 */  [RPL_ENDOFINVITELIST]       = ":%s 347 %s %s :End of Channel Invite List",
  /* 348 */  [RPL_EXCEPTLIST]           = ":%s 348 %s %s %s %s %lu",
  /* 349 */  [RPL_ENDOFEXCEPTLIST]      = ":%s 349 %s %s :End of Channel Exception List",
  /* 351 */  [RPL_VERSION]              = ":%s 351 %s %s(%s). %s :%s",
  /* 352 */  [RPL_WHOREPLY]             = ":%s 352 %s %s %s %s %s %s %s :%d %s",
  /* 353 */  [RPL_NAMREPLY]             = ":%s 353 %s %s %s :",
  /* 362 */  [RPL_CLOSING]              = ":%s 362 %s %s :Closed. Status = %d",
  /* 363 */  [RPL_CLOSEEND]             = ":%s 363 %s %d: Connections Closed",
  /* 364 */  [RPL_LINKS]                = ":%s 364 %s %s %s :%d %s",
  /* 365 */  [RPL_ENDOFLINKS]           = ":%s 365 %s %s :End of /LINKS list.",
  /* 366 */  [RPL_ENDOFNAMES]           = ":%s 366 %s %s :End of /NAMES list.",
  /* 367 */  [RPL_BANLIST]              = ":%s 367 %s %s %s %s %lu",
  /* 368 */  [RPL_ENDOFBANLIST]         = ":%s 368 %s %s :End of Channel Ban List",
  /* 369 */  [RPL_ENDOFWHOWAS]          = ":%s 369 %s %s :End of WHOWAS",
  /* 371 */  [RPL_INFO]                 = ":%s 371 %s :%s",
  /* 372 */  [RPL_MOTD]                 = ":%s 372 %s :- %s",
  /* 373 */  [RPL_INFOSTART]            = ":%s 373 %s :Server INFO",
  /* 374 */  [RPL_ENDOFINFO]            = ":%s 374 %s :End of /INFO list.",
  /* 375 */  [RPL_MOTDSTART]            = ":%s 375 %s :- %s Message of the Day - ",
  /* 376 */  [RPL_ENDOFMOTD]            = ":%s 376 %s :End of /MOTD command.",
  /* 379 */  [RPL_WHOISMODES]           = ":%s 379 %s %s :is using modes %s authflags: %s",
  /* 381 */  [RPL_YOUREOPER]            = ":%s 381 %s :You have entered... the Twilight Zone!",
  /* 382 */  [RPL_REHASHING]            = ":%s 382 %s %s :Rehashing",
  /* 386 */  [RPL_RSACHALLENGE]         = ":%s 386 %s :%s",
  /* 391 */  [RPL_TIME]                 = ":%s 391 %s %s :%s",
  /* 396 */  [RPL_VISIBLEHOST]          = ":%s 396 %s %s :is now your visible host",
  /* 401 */  [ERR_NOSUCHNICK]           = ":%s 401 %s %s :No such nick/channel",
  /* 402 */  [ERR_NOSUCHSERVER]         = ":%s 402 %s %s :No such server",
  /* 403 */  [ERR_NOSUCHCHANNEL]        = ":%s 403 %s %s :No such channel",
  /* 404 */  [ERR_CANNOTSENDTOCHAN]     = ":%s 404 %s %s :Cannot send to channel",
  /* 405 */  [ERR_TOOMANYCHANNELS]      = ":%s 405 %s %s :You have joined too many channels",
  /* 406 */  [ERR_WASNOSUCHNICK]        = ":%s 406 %s %s :There was no such nickname",
  /* 407 */  [ERR_TOOMANYTARGETS]       = ":%s 407 %s %s :Too many recipients. Only %d processed",
  /* 408 */  [ERR_NOCTRLSONCHAN]        = ":%s 408 %s %s :You cannot use control codes on this channel. Not sent: %s",
  /* 409 */  [ERR_NOORIGIN]             = ":%s 409 %s :No origin specified",
  /* 410 */  [ERR_INVALIDCAPCMD]        = ":%s 410 %s %s :Invalid CAP subcommand",
  /* 411 */  [ERR_NORECIPIENT]          = ":%s 411 %s :No recipient given (%s)",
  /* 412 */  [ERR_NOTEXTTOSEND]         = ":%s 412 %s :No text to send",
  /* 413 */  [ERR_NOTOPLEVEL]           = ":%s 413 %s %s :No toplevel domain specified",
  /* 414 */  [ERR_WILDTOPLEVEL]         = ":%s 414 %s %s :Wildcard in toplevel Domain",
  /* 421 */  [ERR_UNKNOWNCOMMAND]       = ":%s 421 %s %s :Unknown command",
  /* 422 */  [ERR_NOMOTD]               = ":%s 422 %s :MOTD File is missing",
  /* 423 */  [ERR_NOADMININFO]          = ":%s 423 %s %s :No administrative info available",
  /* 431 */  [ERR_NONICKNAMEGIVEN]      = ":%s 431 %s :No nickname given",
  /* 432 */  [ERR_ERRONEUSNICKNAME]     = ":%s 432 %s %s :%s",
  /* 433 */  [ERR_NICKNAMEINUSE]        = ":%s 433 %s %s :Nickname is already in use.",
  /* 436 */  [ERR_NICKCOLLISION]        = ":%s 436 %s %s :Nickname collision KILL",
  /* 437 */  [ERR_BANNICKCHANGE]        = ":%s 437 %s %s :Cannot change nickname while banned on channel",
  /* 438 */  [ERR_NICKTOOFAST]          = ":%s 438 %s %s %s :Nick change too fast. Please wait %d seconds.",
  /* 440 */  [ERR_SERVICESDOWN]         = ":%s 440 %s %s :Services are currently offline.",
  /* 441 */  [ERR_USERNOTINCHANNEL]     = ":%s 441 %s %s %s :They aren't on that channel",
  /* 442 */  [ERR_NOTONCHANNEL]         = ":%s 442 %s %s :You're not on that channel",
  /* 443 */  [ERR_USERONCHANNEL]        = ":%s 443 %s %s %s :is already on channel",
  /* 451 */  [ERR_NOTREGISTERED]        = ":%s 451 %s :You have not registered",
  /* 456 */  [ERR_ACCEPTFULL]           = ":%s 456 %s :Accept list is full",
  /* 457 */  [ERR_ACCEPTEXIST]          = ":%s 457 %s %s!%s@%s :is already on your accept list",
  /* 458 */  [ERR_ACCEPTNOT]            = ":%s 458 %s %s!%s@%s :is not on your accept list",
  /* 461 */  [ERR_NEEDMOREPARAMS]       = ":%s 461 %s %s :Not enough parameters",
  /* 462 */  [ERR_ALREADYREGISTRED]     = ":%s 462 %s :You may not reregister",
  /* 464 */  [ERR_PASSWDMISMATCH]       = ":%s 464 %s :Password Incorrect",
  /* 465 */  [ERR_YOUREBANNEDCREEP]     = ":%s 465 %s :You are banned from this server- %s",
  /* 468 */  [ERR_ONLYSERVERSCANCHANGE] = ":%s 468 %s %s :Only servers can change that mode",
  /* 470 */  [ERR_LINKCHANNEL]          = ":%s 470 %s %s %s :You are being forwarded from %s to linked channel %s",
  /* 471 */  [ERR_CHANNELISFULL]        = ":%s 471 %s %s :Cannot join channel (+l)",
  /* 472 */  [ERR_UNKNOWNMODE]          = ":%s 472 %s %c :is unknown mode char to me",
  /* 473 */  [ERR_INVITEONLYCHAN]       = ":%s 473 %s %s :Cannot join channel (+i)",
  /* 474 */  [ERR_BANNEDFROMCHAN]       = ":%s 474 %s %s :Cannot join channel (+b)",
  /* 475 */  [ERR_BADCHANNELKEY]        = ":%s 475 %s %s :Cannot join channel (+k)",
  /* 477 */  [ERR_NEEDREGGEDNICK]       = ":%s 477 %s %s :You need to login to services to join or speak in that channel.",
  /* 478 */  [ERR_BANLISTFULL]          = ":%s 478 %s %s %s :Channel ban list is full",
  /* 479 */  [ERR_BADCHANNAME]          = ":%s 479 %s %s :Illegal channel name",
  /* 480 */  [ERR_SSLONLYCHAN]          = ":%s 480 %s %s :Cannot join channel (+S) - This channel requires a secure connection: https://s.rizon.net/tls",
  /* 481 */  [ERR_NOPRIVILEGES]         = ":%s 481 %s :Permission Denied - You're not an IRC operator",
  /* 482 */  [ERR_CHANOPRIVSNEEDED]     = ":%s 482 %s %s :You're not channel operator",
  /* 483 */  [ERR_CANTKILLSERVER]       = ":%s 483 %s :You cannot kill a server!",
  /* 484 */  [ERR_RESTRICTED]           = ":%s 484 %s :You are restricted",
  /* 485 */  [ERR_CHANBANREASON]        = ":%s 485 %s %s :Cannot join channel (%s)",
  /* 486 */  [ERR_NONONREG]             = ":%s 486 %s %s :You must login to services to private message that person",
  /* 491 */  [ERR_NOOPERHOST]           = ":%s 491 %s :Only few of mere mortals may try to enter the twilight zone",
  /* 492 */  [ERR_NOCTCP]               = ":%s 492 %s %s :You cannot send CTCPs to this %s. Not sent: %s",
  /* 499 */  [ERR_CHANOWNPRIVNEEDED]    = ":%s 499 %s %s :You're not a channel owner",
  /* 501 */  [ERR_UMODEUNKNOWNFLAG]     = ":%s 501 %s :Unknown MODE flag",
  /* 502 */  [ERR_USERSDONTMATCH]       = ":%s 502 %s :Cannot change mode for other users",
  /* 504 */  [ERR_USERNOTONSERV]        = ":%s 504 %s %s :User is not on this server",
  /* 512 */  [ERR_TOOMANYWATCH]         = ":%s 512 %s %s :Maximum size for WATCH-list is %d entries",
  /* 513 */  [ERR_WRONGPONG]            = ":%s 513 %s :To connect type /QUOTE PONG %u",
  /* 520 */  [ERR_OPERONLYCHAN]         = ":%s 520 %s %s :Cannot join channel (+O)",
  /* 521 */  [ERR_LISTSYNTAX]           = ":%s 521 %s :Bad list syntax, type /QUOTE HELP LIST",
  /* 524 */  [ERR_HELPNOTFOUND]         = ":%s 524 %s %s :Help not found",
  /* 600 */  [RPL_LOGON]                = ":%s 600 %s %s %s %s %d :logged online",
  /* 601 */  [RPL_LOGOFF]               = ":%s 601 %s %s %s %s %d :logged offline",
  /* 602 */  [RPL_WATCHOFF]             = ":%s 602 %s %s %s %s %d :stopped watching",
  /* 603 */  [RPL_WATCHSTAT]            = ":%s 603 %s :You have %u and are on %u WATCH entries",
  /* 604 */  [RPL_NOWON]                = ":%s 604 %s %s %s %s %d :is online",
  /* 605 */  [RPL_NOWOFF]               = ":%s 605 %s %s %s %s %d :is offline",
  /* 606 */  [RPL_WATCHLIST]            = ":%s 606 %s :%s",
  /* 607 */  [RPL_ENDOFWATCHLIST]       = ":%s 607 %s :End of WATCH %c",
  /* 671 */  [RPL_WHOISSECURE]          = ":%s 671 %s %s :is using a secure connection",
  /* 672 */  [RPL_WHOISREALIP]          = ":%s 672 %s %s :is actually from %s [%s]",
  /* 702 */  [RPL_MODLIST]              = ":%s 702 %s %s %p %s %s",
  /* 703 */  [RPL_ENDOFMODLIST]         = ":%s 703 %s :End of /MODLIST.",
  /* 704 */  [RPL_HELPSTART]            = ":%s 704 %s %s :%s",
  /* 705 */  [RPL_HELPTXT]              = ":%s 705 %s %s :%s",
  /* 706 */  [RPL_ENDOFHELP]            = ":%s 706 %s %s :End of /HELP.",
  /* 707 */  [ERR_TARGCHANGE]           = ":%s 707 %s %s :Targets changing too fast, message dropped. Please wait %ld seconds.",
  /* 709 */  [RPL_ETRACE]               = ":%s 709 %s %s %s %s %s %s %s %s %s :%s",
  /* 710 */  [RPL_KNOCK]                = ":%s 710 %s %s %s!%s@%s :has asked for an invite.",
  /* 711 */  [RPL_KNOCKDLVR]            = ":%s 711 %s %s :Your KNOCK has been delivered.",
  /* 712 */  [ERR_TOOMANYKNOCK]         = ":%s 712 %s %s :Too many KNOCKs (%s).",
  /* 713 */  [ERR_CHANOPEN]             = ":%s 713 %s %s :Channel is open.",
  /* 714 */  [ERR_KNOCKONCHAN]          = ":%s 714 %s %s :You are already on that channel.",
  /* 715 */  [RPL_INVITETHROTTLE]       = ":%s 715 %s %s %s :You are inviting too fast, invite to %s for %s not sent.",
  /* 716 */  [RPL_TARGUMODEG]           = ":%s 716 %s %s :is in %s mode (%s)",
  /* 717 */  [RPL_TARGNOTIFY]           = ":%s 717 %s %s :has been informed that you messaged them.",
  /* 718 */  [RPL_UMODEGMSG]            = ":%s 718 %s %s %s@%s :is messaging you, and you are umode %s.",
  /* 723 */  [ERR_NOPRIVS]              = ":%s 723 %s %s :Insufficient oper privs.",
  /* 727 */  [RPL_ISCAPTURED]           = ":%s 727 %s %s :is captured",
  /* 728 */  [RPL_ISUNCAPTURED]         = ":%s 728 %s %s :is uncaptured",
  /* 743 */  [ERR_INVALIDBAN]           = ":%s 743 %s %s %s :Invalid ban mask",
  /* 900 */  [RPL_LOGGEDIN]             = ":%s 900 %s %s!%s@%s %s :You are now logged in as %s.",
  /* 903 */  [RPL_SASLSUCCESS]          = ":%s 903 %s :SASL authentication successful",
  /* 904 */  [ERR_SASLFAIL]             = ":%s 904 %s :SASL authentication failed",
  /* 905 */  [ERR_SASLTOOLONG]          = ":%s 905 %s :SASL message too long",
  /* 906 */  [ERR_SASLABORTED]          = ":%s 906 %s :SASL authentication aborted",
  /* 907 */  [ERR_SASLALREADY]          = ":%s 907 %s :You have already completed SASL authentication",
  /* 908 */  [RPL_SASLMECHS]            = ":%s 908 %s %s :are available SASL mechanisms",
  /* 972 */  [ERR_CANNOTDOCOMMAND]      = ":%s 972 %s %s :%s",
  /* 974 */  [ERR_CANNOTCHANGECHANMODE] = ":%s 974 %s %c :%s",
  /* 999 */  [ERR_LAST_ERR_MSG]         = ":%s 999 %s :Last Error Message"
};

/*
 * form_str
 *
 * inputs       - numeric
 * output       - corresponding string
 * side effects - NONE
 */
const char *
form_str(enum irc_numerics numeric)
{
  assert(numeric > 0 && numeric < ERR_LAST_ERR_MSG);

  if (numeric > ERR_LAST_ERR_MSG || !replies[numeric])
    numeric = ERR_LAST_ERR_MSG;

  assert(replies[numeric]);

  return replies[numeric];
}
