/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2015-2016 plexus development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

extern void ssl_init(void);
extern struct sslprofile *sslprofile_create(void);
extern struct sslprofile_identity *sslprofile_identity_create(struct sslprofile *);
extern void sslprofile_free(struct sslprofile *);
extern struct sslprofile *sslprofile_default(void);
extern struct sslprofile *sslprofile_find(const char *);
extern struct sslprofile_identity *sslprofile_find_identity(struct sslprofile *, const char *);
extern struct sslprofile_identity *sslprofile_identity_get_default_from_profile(const char *profile_name);
extern struct sslprofile_identity *sslprofile_identity_get_default(struct sslprofile *);
extern const char *ssl_get_err(void);