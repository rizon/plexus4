/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2016 Adam <Adam@anope.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "stdinc.h"
#include "ircd_defs.h"
#include "list.h"

#ifdef HAVE_LIBMICROHTTPD
# include <microhttpd.h>

struct HttpResource
{
  const char *method;
  const char *url;
  int (*handler)(struct MHD_Connection *connection, const char *url,
                const char *method, const char *version, const char *upload_data,
                size_t *upload_data_size);
  dlink_node node;
};

extern void httpd_register(struct HttpResource *);
extern void httpd_unregister(struct HttpResource *);

#endif

extern void httpd_start(void);
extern void httpd_stop(void);
extern int httpd_add_connection(int, struct irc_ssaddr *);

