/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 1997-2014 ircd-hybrid development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

/*! \file m_pong.c
 * \brief Includes required functions for processing the PONG command.
 * \version $Id$
 */

#include "stdinc.h"
#include "ircd.h"
#include "s_user.h"
#include "client.h"
#include "hash.h"
#include "numeric.h"
#include "conf.h"
#include "send.h"
#include "irc_string.h"
#include "parse.h"
#include "modules.h"
#include "s_serv.h"

static void
mr_pong(struct Client *client_p, struct Client *source_p,
        int parc, char *parv[])
{
  assert(source_p == client_p);

  if (parc == 2 && *parv[1] != '\0')
  {
    if (ConfigFileEntry.ping_cookie && source_p->localClient->random_ping)
    {
      unsigned int incoming_ping = strtoul(parv[1], NULL, 10);

      if (source_p->localClient->random_ping == incoming_ping)
      {
        SetPingCookie(source_p);
        if (!source_p->localClient->registration)
          register_local_user(source_p);
      }
      else
        sendto_one(source_p, form_str(ERR_WRONGPONG), me.name,
                   source_p->name, source_p->localClient->random_ping);
    }
  }
  else
    sendto_one(source_p, form_str(ERR_NOORIGIN),
               me.name, source_p->name);
}

static void
ms_pong(struct Client *client_p, struct Client *source_p,
        int parc, char *parv[])
{
  if (parc < 2 || EmptyString(parv[1]))
  {
    sendto_one(source_p, form_str(ERR_NOORIGIN),
               me.name, source_p->name);
    return;
  }

  const char *origin = parv[1];
  const char *destination = parv[2];

  if (EmptyString(destination))
  {
    return;
  }

  struct Client *target_p = hash_find_id(destination);
  if (target_p == NULL)
    target_p = hash_find_client(destination);

  if (target_p == NULL)
  {
    if (!IsDigit(*destination))
      sendto_one(source_p, form_str(ERR_NOSUCHSERVER),
                 me.name, source_p->name, destination);
    return;
  }

  if (target_p->from == client_p || IsMe(target_p))
  {
    return;
  }

  sendto_one(target_p, ":%s PONG %s %s",
             ID_or_name(source_p, target_p), origin, target_p->name);
}

static struct Message pong_msgtab =
{
  .cmd = "PONG",
  .args_min = 1,
  .args_max = MAXPARA,
  .handlers[UNREGISTERED_HANDLER] = mr_pong,
  .handlers[CLIENT_HANDLER] = m_ignore,
  .handlers[SERVER_HANDLER] = ms_pong,
  .handlers[ENCAP_HANDLER] = m_ignore,
  .handlers[OPER_HANDLER] = m_ignore,
};

static void
module_init(void)
{
  mod_add_cmd(&pong_msgtab);
}

static void
module_exit(void)
{
  mod_del_cmd(&pong_msgtab);
}

struct module module_entry =
{
  .version = "$Revision$",
  .modinit = module_init,
  .modexit = module_exit,
};
