/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 1997-2014 ircd-hybrid development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

/*! \file m_ping.c
 * \brief Includes required functions for processing the PING command.
 * \version $Id$
 */

#include "stdinc.h"
#include "client.h"
#include "ircd.h"
#include "numeric.h"
#include "send.h"
#include "irc_string.h"
#include "parse.h"
#include "modules.h"
#include "hash.h"
#include "conf.h"
#include "s_serv.h"


/*
** m_ping
**      parv[0] = sender prefix
**      parv[1] = origin
**      parv[2] = destination
*/
static void
m_ping(struct Client *client_p, struct Client *source_p,
       int parc, char *parv[])
{
  struct Client *target_p = NULL;
  char *origin, *destination;

  if (parc < 2 || EmptyString(parv[1]))
  {
    sendto_one(source_p, form_str(ERR_NOORIGIN),
               me.name, source_p->name);
    return;
  }

  origin = parv[1];
  destination = parv[2];  /* Will get NULL or pointer (parc >= 2!!) */

  if (ConfigServerHide.disable_remote_commands && !HasUMode(source_p, UMODE_OPER))
  {
    sendto_one(source_p, ":%s PONG %s :%s", me.name,
              (destination) ? destination : me.name, origin);
    return;
  }

  if (EmptyString(destination) || ((target_p = find_server(source_p, destination)) && IsMe(target_p)))
  {
    sendto_one(source_p, ":%s PONG %s :%s", me.name, me.name, origin);
  }
  else if (target_p)
  {
    sendto_one(target_p, ":%s PING %s :%s",
               ID_or_name(source_p, target_p), source_p->name,
               ID_or_name(target_p, target_p));
  }
  else
  {
    sendto_one(source_p, form_str(ERR_NOSUCHSERVER),
               me.name, source_p->name, destination);
  }
}

static void
ms_ping(struct Client *client_p, struct Client *source_p,
        int parc, char *parv[])
{
  struct Client *target_p = NULL;
  const char *destination;

  if (parc < 2 || EmptyString(parv[1]))
  {
    sendto_one(source_p, form_str(ERR_NOORIGIN),
               me.name, source_p->name);
    return;
  }

  destination = parv[2];  /* Will get NULL or pointer (parc >= 2!!) */

  if (EmptyString(destination) || ((target_p = hash_find_server(destination)) && IsMe(target_p)))
  {
    sendto_one(source_p, ":%s PONG %s :%s",
               ID_or_name(&me, source_p), me.name, ID_or_name(source_p, source_p));
  }
  else if (target_p)
  {
    if (target_p->from != client_p)
      sendto_one(target_p, ":%s PING %s :%s",
                 ID_or_name(source_p, target_p), source_p->name,
                 ID_or_name(target_p, target_p));
  }
  else
  {
    sendto_one(source_p, form_str(ERR_NOSUCHSERVER),
               ID_or_name(&me, source_p),
               source_p->name, destination);
  }
}

static struct Message ping_msgtab =
{
  .cmd = "PING",
  .args_min = 1,
  .args_max = MAXPARA,
  .flags = MFLG_CAPTURED,
  .handlers[UNREGISTERED_HANDLER] = m_unregistered,
  .handlers[CLIENT_HANDLER] = m_ping,
  .handlers[SERVER_HANDLER] = ms_ping,
  .handlers[ENCAP_HANDLER] = m_ignore,
  .handlers[OPER_HANDLER] = m_ping,
};

static void
module_init(void)
{
  mod_add_cmd(&ping_msgtab);
}

static void
module_exit(void)
{
  mod_del_cmd(&ping_msgtab);
}

struct module module_entry = {
  .version = "$Revision$",
  .modinit = module_init,
  .modexit = module_exit,
};
