/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2012-2014 ircd-hybrid development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

/*! \file m_webirc.c
 * \brief Includes required functions for processing the WEBIRC command.
 * \version $Id$
 */

#include "stdinc.h"
#include "list.h"
#include "client.h"
#include "ircd.h"
#include "send.h"
#include "irc_string.h"
#include "parse.h"
#include "modules.h"
#include "conf.h"
#include "hostmask.h"
#include "dnsbl.h"
#include "memory.h"
#include "s_user.h"
#include "listener.h"
#include "s_serv.h"
#include "hash.h"
#include "s_misc.h"

static void
webirc_apply(struct Client *target_p, const char *host, const char *ip, const char *password)
{
  struct addrinfo hints, *res;

  assert(IsUnknown(target_p));
  assert(MyConnect(target_p));

  if (!valid_hostname(host))
    return;

  memset(&hints, 0, sizeof(hints));

  hints.ai_family   = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags    = AI_PASSIVE | AI_NUMERICHOST;

  if (getaddrinfo(ip, NULL, &hints, &res))
    return;

  assert(res != NULL);

  clear_dnsbl_lookup(target_p);

  /* store original host */
  if (target_p->cgisockhost == NULL)
    target_p->cgisockhost = xstrdup(target_p->sockhost);
  if (target_p->cgihost == NULL)
    target_p->cgihost = xstrdup(target_p->realhost);

  /* apply new ip and host */
  strlcpy(target_p->sockhost, ip, sizeof(target_p->sockhost));
  strlcpy(target_p->host, host, sizeof(target_p->host));
  strlcpy(target_p->realhost, host, sizeof(target_p->realhost));

  if (target_p->sockhost[0] == ':')
  {
    memmove(target_p->sockhost + 1, target_p->sockhost, sizeof(target_p->sockhost) - 1);
    target_p->sockhost[0] = '0';
  }

  /* apply webirc password to client */
  MyFree(target_p->localClient->passwd);
  target_p->localClient->passwd = xstrndup(password, IRCD_MIN(strlen(password), PASSWDLEN));

  memcpy(&target_p->localClient->ip, res->ai_addr, res->ai_addrlen);
  target_p->localClient->ip.ss_len = res->ai_addrlen;
  target_p->localClient->ip.ss.ss_family = res->ai_family;
  target_p->localClient->aftype = res->ai_family;

  freeaddrinfo(res);

  check_klines(target_p); /* this is to check dlines */
  if (IsDefunct(target_p))
    return;

  AddUMode(target_p, UMODE_WEBIRC);

  /* Restart DNSBL lookups for the new IP */
  start_dnsbl_lookup(target_p);
}

/*
 * mr_webirc
 *      parv[0] = sender prefix
 *      parv[1] = password
 *      parv[2] = fake username (we ignore this)
 *      parv[3] = fake hostname
 *      parv[4] = fake ip
 */
static void
mr_webirc(struct Client *client_p, struct Client *source_p, int parc, char *parv[])
{
  struct MaskItem *conf = NULL;
  struct addrinfo hints, *res;
  const char *host = parv[3];

  assert(source_p == client_p);

  if (!client_p->localClient->listener || IsListenerServer(client_p->localClient->listener))
  {
    exit_client(source_p, &me, "Use a different port");
    return;
  }

  if (!valid_hostname(host))
  {
    /* hostname too long? */
    host = parv[4];

    if (!valid_hostname(host))
    {
      sendto_one(source_p, ":%s NOTICE %s :WEBIRC: Invalid hostname", me.name,
                 source_p->name[0] ? source_p->name : "*");
      exit_client(source_p, &me, "WEBIRC: Invalid hostname");
      return;
    }

    sendto_one(source_p, ":%s NOTICE %s :WEBIRC: Hostname is invalid, using your IP address instead", me.name,
               source_p->name[0] ? source_p->name : "*");
  }

  memset(&hints, 0, sizeof(hints));

  hints.ai_family   = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags    = AI_PASSIVE | AI_NUMERICHOST;

  if (getaddrinfo(parv[4], NULL, &hints, &res))
  {
    sendto_one(source_p, ":%s NOTICE %s :WEBIRC: Invalid IP %s", me.name,
               source_p->name[0] ? source_p->name : "*", parv[4]);
    exit_client(source_p, &me, "WEBIRC: Invalid IP");
    return;
  }

  assert(res != NULL);
  freeaddrinfo(res);

  struct MaskLookup lookup = {
    .client = source_p,
    .name = source_p->realhost,
    .ip = source_p->sockhost,
    .addr = &source_p->localClient->ip,
    .fam = source_p->localClient->aftype,
    .username = IsGotId(source_p) ? source_p->username : "webirc",
    .password = parv[1],
    .cmpfunc = match
  };

  conf = find_conf_by_address(CONF_CLIENT, &lookup);

  if (conf == NULL || !IsConfClient(conf) || !IsConfWebIRC(conf))
  {
    struct MaskLookup wlinelookup = {
      .name     = source_p->realhost,
      .addr     = &source_p->localClient->ip,
      .fam      = source_p->localClient->aftype,
      .cmpfunc  = match
    };

    // Unable to find in client config, try wline config.
    conf = find_conf_by_address(CONF_WLINE, &wlinelookup);

    if (conf == NULL)
    {
      sendto_one(source_p, ":%s NOTICE %s :WEBIRC: access denied (no auth block, or bad password)",
                  me.name, source_p->name[0] ? source_p->name : "*");
      exit_client(source_p, &me, "WEBIRC: access denied");
      return;
    }

    if (HasFlag(conf, CONF_FLAGS_WEBIRC_CLOSED))
    {
      sendto_one(source_p, ":%s NOTICE %s :WEBIRC: webirc block is full",
                  me.name, source_p->name[0] ? source_p->name : "*");
      exit_client(source_p, &me, "WEBIRC: webirc block is full");
      return;
    }
  }

  /* find_address_conf won't check password with need_password */
  if (EmptyString(conf->passwd) || !match_conf_password(parv[1], conf))
  {
    sendto_one(source_p, ":%s NOTICE %s :WEBIRC: password incorrect",
               me.name, source_p->name[0] ? source_p->name : "*");
    exit_client(source_p, &me, "WEBIRC: password incorrect");
    return;
  }

  webirc_apply(source_p, host, parv[4], parv[1]);
}

static struct MaskItem *
find_webirc(const char *address)
{
  struct irc_ssaddr addr;
  int bits;

  int masktype = parse_netmask(address, &addr, &bits);

  struct MaskLookup lookup = {
    .name     = address,
    .fam      = masktype != HM_HOST ? addr.ss.ss_family : 0,
    .addr     = masktype != HM_HOST ? &addr : NULL,
    .cmpfunc  = irccmp
  };

  return find_conf_by_address(CONF_WLINE, &lookup);
}

/*
 * swebirc_add
 *      parv[1] = operation SET    - ignored
 *      parv[2] = webirc host
 *      parv[3] = webirc password
 *      parv[4] = webirc creator
 *      parv[5] = webirc created
 *      parv[6] = webirc expires
 *      parv[7] = webirc reason
 */
static void
swebirc_add(struct Client *client_p, struct Client *source_p, int parc, char *parv[])
{
  const char *mask       = parv[2];
  const char *password   = parv[3];
  const char *creator    = parv[4];
  const char *createdstr = parv[5];
  const char *expirestr  = parv[6];
  const char *reason     = parv[parc - 1];

  struct Client *server = IsServer(source_p) ? source_p : source_p->servptr;

  if (!find_matching_name_conf(CONF_ULINE, server->name,
                               NULL,
                               NULL,
                               SHARED_WLINE))
  {
    return;
  }

  struct MaskItem *conf = find_webirc(mask);

  if (conf != NULL)
  {
    delete_one_address_conf(conf->host, conf);
  }

  conf = conf_make(CONF_WLINE);

  conf->passwd = xstrdup(password);
  conf->user   = xstrdup(creator);
  conf->host   = xstrdup(mask);
  conf->setat  = atol(createdstr);
  conf->until  = atol(expirestr);
  conf->reason = xstrdup(reason);

  // XXX hack to make find_conf_by_address not try to match the webirc
  // password against what we provide in the lookup
  AddFlag(conf, CONF_FLAGS_NEED_PASSWORD);

  SetConfDatabase(conf);

  add_conf_by_address(CONF_WLINE, conf);

  sendto_snomask(SNO_DEBUG, L_ALL,
                 "%s added wline for %s, to expire on %s",
                 source_p->name, conf->host, conf->until != 0 ? myctime(conf->until) : "never");
}

/*
 * swebirc_remove
 *      parv[1] = operation UNSET - ignored
 *      parv[2] = webirc host
 */
static void
swebirc_remove(struct Client *client_p, struct Client *source_p, int parc, char *parv[])
{
  const char *mask = parv[2];

  struct Client *server = IsServer(source_p) ? source_p : source_p->servptr;

  if (!find_matching_name_conf(CONF_ULINE, server->name,
                               NULL,
                               NULL,
                               SHARED_WLINE))
  {
    return;
  }

  struct MaskItem *conf = find_webirc(mask);

  if (conf == NULL)
  {
    // Does not exist
    return;
  }

  sendto_snomask(SNO_DEBUG, L_ALL,
                 "%s removed wline for %s",
                 source_p->name, conf->host);

  delete_one_address_conf(conf->host, conf);
}

/*
 * swebirc_open
 *      parv[1] = operation OPEN - ignored
 *      parv[2] = webirc host
 */
static void
swebirc_open(struct Client *client_p, struct Client *source_p, int parc, char *parv[])
{
  const char *mask     = parv[2];

  struct Client *server = IsServer(source_p) ? source_p : source_p->servptr;

  if (!find_matching_name_conf(CONF_ULINE, server->name,
                               NULL,
                               NULL,
                               SHARED_WLINE))
  {
    return;
  }

  struct MaskItem *conf = find_webirc(mask);

  if (conf == NULL)
  {
    // Does not exist
    return;
  }

  if (!HasFlag(conf, CONF_FLAGS_WEBIRC_CLOSED))
  {
    // Entry is not closed.
    return;
  }

  sendto_snomask(SNO_DEBUG, L_ALL,
                 "%s opened wline for %s",
                 source_p->name, conf->host);

  DelFlag(conf, CONF_FLAGS_WEBIRC_CLOSED);
}

/*
 * swebirc_close
 *      parv[1] = operation CLOSE - ignored
 *      parv[2] = webirc host
 */
static void
swebirc_close(struct Client *client_p, struct Client *source_p, int parc, char *parv[])
{
  const char *mask = parv[2];

  struct Client *server = IsServer(source_p) ? source_p : source_p->servptr;

  if (!find_matching_name_conf(CONF_ULINE, server->name,
                               NULL,
                               NULL,
                               SHARED_WLINE))
  {
    return;
  }

  struct MaskItem *conf = find_webirc(mask);

  if (conf == NULL)
  {
    // Does not exist
    return;
  }

  if (HasFlag(conf, CONF_FLAGS_WEBIRC_CLOSED))
  {
    // Entry already closed
    return;
  }

  sendto_snomask(SNO_DEBUG, L_ALL,
                 "%s closed wline for %s",
                 source_p->name, conf->host);

  AddFlag(conf, CONF_FLAGS_WEBIRC_CLOSED);
}

/*
 * me_swebirc
 *      parv[1] = operation (SET, UNSET, OPEN, CLOSE)
 *      parv[2] = webirc host
 *      parv[3] = webirc password
 *      parv[4] = webirc creator
 *      parv[5] = webirc created
 *      parv[6] = webirc expires
 *      parv[7] = webirc reason
 */
static void
me_swebirc(struct Client *client_p, struct Client *source_p, int parc, char *parv[])
{
  const char *operation = parv[1];

  if (!strcmp(operation, "SET") && parc == 8)
  {
    swebirc_add(client_p, source_p, parc, parv);
  }
  else if (!strcmp(operation, "UNSET"))
  {
    swebirc_remove(client_p, source_p, parc, parv);
  }
  else if (!strcmp(operation, "OPEN"))
  {
    swebirc_open(client_p, source_p, parc, parv);
  }
  else if (!strcmp(operation, "CLOSE"))
  {
    swebirc_close(client_p, source_p, parc, parv);
  }
}

/*
 * me_uwebirc
 *      parv[1] = user
 *      parv[2] = webirc sockhost
 *      parv[3] = webirc realhost
 */
static void
me_uwebirc(struct Client *client_p, struct Client *source_p, int parc, char *parv[])
{
  struct Client *target_p = find_person(client_p, parv[1]);
  if (target_p == NULL)
    return;

  MyFree(target_p->cgisockhost);
  target_p->cgisockhost = NULL;

  MyFree(target_p->cgihost);
  target_p->cgihost = NULL;

  if (parc > 3 && !EmptyString(parv[3]))
  {
    target_p->cgisockhost = xstrdup(parv[2]);
    target_p->cgihost = xstrdup(parv[3]);
  }
}

static struct Message webirc_msgtab =
{
  .cmd = "WEBIRC",
  .args_min = 5,
  .args_max = MAXPARA,
  .handlers[UNREGISTERED_HANDLER] = mr_webirc,
  .handlers[CLIENT_HANDLER] = m_registered,
  .handlers[SERVER_HANDLER] = m_ignore,
  .handlers[ENCAP_HANDLER] = m_ignore,
  .handlers[OPER_HANDLER] = m_registered,
};

static struct Message swebirc_msgtab =
{
  .cmd = "SWEBIRC",
  .args_min = 3,
  .args_max = MAXPARA,
  .handlers[UNREGISTERED_HANDLER] = m_unregistered,
  .handlers[CLIENT_HANDLER] = m_ignore,
  .handlers[SERVER_HANDLER] = m_ignore,
  .handlers[ENCAP_HANDLER] = me_swebirc,
  .handlers[OPER_HANDLER] = m_ignore,
};

static struct Message uwebirc_msgtab =
{
  .cmd = "UWEBIRC",
  .args_min = 2,
  .args_max = MAXPARA,
  .handlers[UNREGISTERED_HANDLER] = m_unregistered,
  .handlers[CLIENT_HANDLER] = m_ignore,
  .handlers[SERVER_HANDLER] = m_ignore,
  .handlers[ENCAP_HANDLER] = me_uwebirc,
  .handlers[OPER_HANDLER] = m_ignore,
};

static void
module_init(void)
{
  mod_add_cmd(&webirc_msgtab);
  mod_add_cmd(&swebirc_msgtab);
  mod_add_cmd(&uwebirc_msgtab);
}

static void
module_exit(void)
{
  mod_del_cmd(&webirc_msgtab);
  mod_del_cmd(&swebirc_msgtab);
  mod_del_cmd(&uwebirc_msgtab);
}

struct module module_entry =
{
  .version = "$Revision$",
  .modinit = module_init,
  .modexit = module_exit,
};
