/*
 *  ircd-hybrid: an advanced Internet Relay Chat Daemon(ircd).
 *  m_shedding.c: Enables/disables user shedding.
 *
 *  Copyright (C) 2002 by the past and present ircd coders, and others.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 *
 *  $Id$
 */

#include "stdinc.h"
#include "list.h"
#include "channel.h"
#include "client.h"
#include "conf.h"
#include "hostmask.h"
#include "numeric.h"
#include "send.h"
#include "parse.h"
#include "modules.h"
#include "event.h"
#include "irc_string.h"
#include "listener.h"
#include "hook.h"
#include "s_serv.h"

#define SHED_RATE_MIN 5

static int shedding;
static int rate;
static struct MaskItem *redir;

static struct MaskItem *
find_redir()
{
  for (int i = 0; i < ATABLE_SIZE; ++i)
  {
    dlink_node *ptr;

    DLINK_FOREACH(ptr, atable[i].head)
    {
      struct AddressRec *arec = ptr->data;
      struct MaskItem *conf = arec->conf;

      if (!IsConfClient(conf) || !IsConfRedir(conf))
        continue;

      if (strcmp(conf->user, "*") || strcmp(conf->host, "*"))
        continue;

      return conf;
    }
  }

  return NULL;
}

static void
send_redir(struct Client *client_p)
{
  if (redir && redir->name)
  {
    int port;

    if (redir->port)
      port = redir->port;
    else
      port = client_p->localClient->listener ? client_p->localClient->listener->port : PORTNUM;

    sendto_one(client_p, form_str(RPL_REDIR),
               me.name, client_p->name,
               redir->name,
               port);
  }
}

static void
user_shedding_shed(void *unused)
{
  dlink_node *ptr;

  DLINK_FOREACH_PREV(ptr, local_client_list.tail)
  {
      struct Client *client_p = ptr->data;

      if (HasUMode(client_p, UMODE_OPER) || IsExemptLimits(client_p))
        continue;

      send_redir(client_p);

      exit_client(client_p, &me, "Server closed connection");
      break;
  }

  eventDelete(user_shedding_shed, NULL);
}

static void
user_shedding_main(void *unused)
{
  int deviation = (rate / (3+(int) (7.0f*rand()/((float) RAND_MAX+1.0f))));

  eventAddIsh("user shedding shed event", user_shedding_shed, NULL, rate+deviation);
}

static void
shed_off()
{
  eventDelete(user_shedding_main, NULL);
  eventDelete(user_shedding_shed, NULL);

  if (redir)
  {
    --redir->ref_count;
    redir = NULL;
  }

  shedding = 0;
  rate = 0;
}

static void
shed_on(int r)
{
  shed_off();

  redir = find_redir();
  if (redir)
  {
    ++redir->ref_count;
  }

  shedding = 1;
  rate = r;

  eventAdd("user shedding main event", user_shedding_main, NULL, rate);
}

static void
m_shedding(struct Client *client_p, struct Client *source_p, int parc, char **parv)
{
  const char *arg = parv[2];

  if (MyClient(source_p) && !HasUMode(source_p, UMODE_ROUTING))
  {
    sendto_one(source_p, form_str(ERR_NOPRIVILEGES), me.name, source_p->name);
    return;
  }

  if (hunt_server(client_p, source_p, ":%s SHEDDING %s :%s", 1, parc, parv) != HUNTED_ISME)
    return;

  if (irccmp(arg, "OFF") == 0)
  {
    shed_off();
    sendto_snomask(SNO_ALL, L_ALL | SNO_ROUTE,
                   "User shedding DISABLED by %s", source_p->name);
    return;
  }

  int new_rate = atoi(arg);
  if (new_rate < SHED_RATE_MIN)
  {
    sendto_one(source_p, ":%s NOTICE %s :Rate may not be less than %d", me.name, source_p->name, SHED_RATE_MIN);
    return;
  }

  shed_off();

  sendto_snomask(SNO_ALL, L_ALL | SNO_ROUTE,
                 "User shedding ENABLED by %s. Shedding interval: %d seconds",
                 source_p->name, new_rate);

  new_rate -= (new_rate/5);

  shed_on(new_rate);
}

static struct Message shedding_msgtab =
{
  .cmd = "SHEDDING",
  .args_min = 3,
  .args_max = MAXPARA,
  .handlers[UNREGISTERED_HANDLER] = m_unregistered,
  .handlers[CLIENT_HANDLER] = m_not_oper,
  .handlers[SERVER_HANDLER] = m_shedding,
  .handlers[ENCAP_HANDLER] = m_ignore,
  .handlers[OPER_HANDLER] = m_shedding,
};

static void
module_init(void)
{
  mod_add_cmd(&shedding_msgtab);
}

static void
module_exit(void)
{
  mod_del_cmd(&shedding_msgtab);

  shed_off();
}

struct module module_entry =
{
  .version = "$Revision$",
  .modinit = module_init,
  .modexit = module_exit,
};

