/*
 *  ircd-hybrid: an advanced Internet Relay Chat Daemon(ircd).
 *  m_chghost.c: Allows changing hostname of connected clients.
 *
 *  Copyright (C) 2002 by the past and present ircd coders, and others.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 *
 *  $Id: m_chghost.c 495 2007-07-08 23:26:42Z jon $
 */

/* List of ircd includes from ../include/ */
#include "stdinc.h"
#include "client.h"
#include "channel.h"
#include "ircd.h"
#include "irc_string.h"
#include "numeric.h"
#include "fdlist.h"
#include "s_bsd.h"
#include "s_serv.h"
#include "send.h"
#include "parse.h"
#include "modules.h"
#include "s_user.h"
#include "hash.h"

static void
me_chgident(struct Client *client_p, struct Client *source_p, int parc, char *parv[])
{
  struct Client *target_p = NULL;

  if(!IsServer(source_p))
    return;

  if((target_p = find_person(client_p, parv[1])) == NULL)
    return;

  if(strlen(parv[2]) > USERLEN || !*parv[2] || !valid_username(parv[2], 0))
    return;

  if(strcmp(target_p->username, parv[2]))
    user_set_hostmask(target_p, parv[2], NULL);
}

static void
mo_chgident(struct Client *client_p, struct Client *source_p, int parc, char *parv[])
{
  struct Client *target_p = NULL;

  if (!HasUMode(source_p, UMODE_NETADMIN))
  {
    sendto_one(source_p, form_str(ERR_NOPRIVILEGES), me.name, source_p->name);
    return;
  }

  if(parc < 3)
  {
    sendto_one(source_p, form_str(ERR_NEEDMOREPARAMS), me.name, source_p->name, "CHGIDENT");
    return;
  }

  if((target_p = find_person(client_p, parv[1])) == NULL)
  {
    sendto_one(source_p, form_str(ERR_NOSUCHNICK),
         me.name, source_p->name, parv[1]);
    return;
  }

  if(strlen(parv[2]) > USERLEN)
  {
    sendto_one(source_p, form_str(ERR_CANNOTDOCOMMAND),
         me.name, source_p->name, "CHGIDENT", "Ident is too long");
    return;
  }

  if(!*parv[2] || !valid_username(parv[2], 1))
  {
    sendto_one(source_p, form_str(ERR_CANNOTDOCOMMAND),
         me.name, source_p->name, "CHGIDENT", "Illegal character in ident");
    return;
  }

  sendto_server(NULL, CAP_ENCAP, NOCAPS,
          ":%s ENCAP * CHGIDENT %s %s",
          me.name, target_p->name, parv[2]);

  if(strcmp(target_p->username, parv[2]))
    user_set_hostmask(target_p, parv[2], NULL);
}

static struct Message chgident_msgtab = {
  .cmd = "CHGIDENT",
  .args_min = 3,
  .args_max = MAXPARA,
  .handlers[UNREGISTERED_HANDLER] = m_unregistered,
  .handlers[CLIENT_HANDLER] = m_ignore,
  .handlers[SERVER_HANDLER] = m_ignore,
  .handlers[ENCAP_HANDLER] = me_chgident,
  .handlers[OPER_HANDLER] = mo_chgident,
};

static void
module_init(void)
{
  mod_add_cmd(&chgident_msgtab);
}

static void
module_exit(void)
{
  mod_del_cmd(&chgident_msgtab);
}

struct module module_entry = {
  .version = "$Revision$",
  .modinit = module_init,
  .modexit = module_exit,
};

