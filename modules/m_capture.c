/*
 *  ircd-hybrid: an advanced Internet Relay Chat Daemon(ircd).
 *  m_capture.c: Makes a designated client captive
 *
 *  Copyright (C) 2002 by the past and present ircd coders, and others.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 *
 *  $Id$
 */

#include "stdinc.h"
#include "client.h"
#include "hash.h"
#include "channel.h"
#include "channel_mode.h"
#include "hash.h"
#include "ircd.h"
#include "numeric.h"
#include "s_serv.h"
#include "send.h"
#include "list.h"
#include "irc_string.h"
#include "parse.h"
#include "modules.h"
#include "conf.h"

/* m_capture
 *      parv[0] = sender prefix
 *      parv[1] = nickname masklist
 *      parv[2] = optional level
 */
static void
m_capture(struct Client *client_p, struct Client *source_p, int parc, char *parv[])
{
  struct Client *target_p = NULL;
  char *p = NULL;
  enum capture_type type;
  const char *level;

  if(parc < 2 || EmptyString(parv[1]))
  {
    sendto_one(source_p, form_str(ERR_NONICKNAMEGIVEN), me.name, source_p->name);
    return;
  }

  /* XXX Add oper flag in future ? */

  if(MyClient(source_p) && !HasUMode(source_p, UMODE_NETADMIN))
  {
    sendto_one(source_p, form_str(ERR_NOPRIVS), me.name, source_p->name, "CAPTURE");
    return;
  }

  if (parc < 3 || EmptyString(parv[2]) || !strcasecmp(parv[2], "full"))
  {
    type = CAPTURE_FULL;
    level = "full";
  }
  else if (!strcasecmp(parv[2], "messages"))
  {
    type = CAPTURE_MESSAGES;
    level = "messages";
  }
  else
  {
    /* unknown type */
    type = CAPTURE_FULL;
    level = "full";
  }

  if((p = strchr(parv[1], '@')) == NULL)
  {
    target_p = find_person(client_p, parv[1]);
    if (target_p == NULL)
    {
      sendto_one(source_p, form_str(ERR_NOSUCHNICK),
           me.name, source_p->name, parv[1]);
      return;
    }

    if (MyClient(source_p))
    {
      if (HasUMode(target_p, UMODE_OPER))
      {
        sendto_one(source_p, form_str(ERR_NOPRIVS),
             me.name, source_p->name, "CAPTURE");
        return;
      }
    }

    if (!MyClient(target_p))
    {
      if (target_p->from != client_p)
        sendto_one(target_p, ":%s CAPTURE %s %s",
                   ID_or_name(source_p, target_p), ID_or_name(target_p, target_p), level);
      return;
    }

    if (!IsCapturedLevel(target_p, type))
    {
      sendto_snomask(SNO_ALL, L_ALL,
                     "Captured %s (%s@%s) [%s] type: %s", target_p->name,
                     target_p->username, target_p->realhost, target_p->info, level);
      SetCapturedLevel(target_p, type);
    }

    sendto_one(source_p, form_str(RPL_ISCAPTURED),
               ID_or_name(&me, source_p), ID_or_name(source_p, source_p), target_p->name);
  }
  else
  {
    const char *nick = NULL, *user = NULL, *host = NULL;
    unsigned int matches = 0;
    dlink_node *ptr = NULL;

    /* p != NULL so user @ host given */
    nick = parv[1];
    *p++ = '\0';
    host = p;

    if((p = strchr(nick, '!')) != NULL)
    {
      *p++ = '\0';
      user = p;
    }
    else
    {
      user = nick;
      nick = "*";
    }

    if(!valid_wild_card(source_p, 3, nick, user, host))
      return;

    sendto_server(client_p, NOCAPS, NOCAPS,
                  ":%s CAPTURE %s!%s@%s %s",
                  source_p->name, nick, user, host, level);

    DLINK_FOREACH(ptr, local_client_list.head)
    {
      target_p = ptr->data;

      if((source_p == target_p) || HasUMode(target_p, UMODE_OPER) || IsCapturedLevel(target_p, type))
        continue;

      if(match(nick, target_p->name) &&
         match(host, target_p->realhost) && match(user, target_p->username))
      {
        SetCapturedLevel(target_p, type);
        ++matches;
      }
    }

    sendto_snomask(SNO_ALL, L_ALL,
                   "Bulk captured %s!%s@%s from %s type: %s, %u local match(es)",
                   nick, user, host, source_p->name, level, matches);
  }
}

/* m_uncapture
 *      parv[0] = sender prefix
 *      parv[1] = nickname masklist
 */
static void
m_uncapture(struct Client *client_p, struct Client *source_p, int parc, char *parv[])
{
  struct Client *target_p = NULL;
  char *p = NULL;

  if(MyClient(source_p) && !HasUMode(source_p, UMODE_NETADMIN))
  {
    sendto_one(source_p, form_str(ERR_NOPRIVS), me.name, source_p->name, "CAPTURE");
    return;
  }

  if(parc < 2 || EmptyString(parv[1]))
  {
    sendto_one(source_p, form_str(ERR_NONICKNAMEGIVEN), me.name, source_p->name);
    return;
  }

  if((p = strchr(parv[1], '@')) == NULL)
  {
    target_p = find_person(client_p, parv[1]);
    if (target_p == NULL)
    {
      sendto_one(source_p, form_str(ERR_NOSUCHNICK),
           me.name, source_p->name, parv[1]);
      return;
    }

    if (!MyClient(target_p))
    {
      if (target_p->from != client_p)
        sendto_one(target_p, ":%s UNCAPTURE %s",
                   ID_or_name(source_p, target_p), ID_or_name(target_p, target_p));
      return;
    }

    if(IsCaptured(target_p))
    {
      ClearCaptured(target_p);

      sendto_snomask(SNO_ALL, L_ALL,
                     "Uncaptured %s (%s@%s) [%s]",
                     target_p->name, target_p->username,
                     target_p->realhost, target_p->info);
    }

    sendto_one(source_p, form_str(RPL_ISUNCAPTURED),
               ID_or_name(&me, source_p), ID_or_name(source_p, source_p), target_p->name);
  }
  else
  {
    const char *nick = NULL, *user = NULL, *host = NULL;
    unsigned int matches = 0;
    dlink_node *ptr = NULL;

    /* p != NULL so user @ host given */
    nick = parv[1];
    *p++ = '\0';
    host = p;

    if((p = strchr(nick, '!')) != NULL)
    {
      *p++ = '\0';
      user = p;
    }
    else
    {
      user = nick;
      nick = "*";
    }

    sendto_server(client_p, NOCAPS, NOCAPS,
                  ":%s UNCAPTURE %s!%s@%s",
                  source_p->name, nick, user, host);

    DLINK_FOREACH(ptr, local_client_list.head)
    {
      target_p = ptr->data;

      if(!IsCaptured(target_p))
        continue;

      if(match(nick, target_p->name) &&
         match(host, target_p->realhost) && match(user, target_p->username))
      {
        ClearCaptured(target_p);
        ++matches;
      }
    }

    sendto_snomask(SNO_ALL, L_ALL,
                   "Bulk uncaptured %s!%s@%s from %s, %u local match(es)",
                   nick, user, host, source_p->name, matches);
  }
}

static struct Message capture_msgtab = {
  .cmd = "CAPTURE",
  .args_max = MAXPARA,
  .handlers[UNREGISTERED_HANDLER] = m_unregistered,
  .handlers[CLIENT_HANDLER] = m_ignore,
  .handlers[SERVER_HANDLER] = m_capture,
  .handlers[ENCAP_HANDLER] = m_capture,
  .handlers[OPER_HANDLER] = m_capture,
};

static struct Message uncapture_msgtab = {
  .cmd = "UNCAPTURE",
  .args_max = MAXPARA,
  .handlers[UNREGISTERED_HANDLER] = m_unregistered,
  .handlers[CLIENT_HANDLER] = m_ignore,
  .handlers[SERVER_HANDLER] = m_uncapture,
  .handlers[ENCAP_HANDLER] = m_uncapture,
  .handlers[OPER_HANDLER] = m_uncapture,
};

static void
module_init(void)
{
  mod_add_cmd(&capture_msgtab);
  mod_add_cmd(&uncapture_msgtab);
}

static void
module_exit(void)
{
  mod_del_cmd(&capture_msgtab);
  mod_del_cmd(&uncapture_msgtab);
}

struct module module_entry = {
  .version = "$Revision$",
  .modinit = module_init,
  .modexit = module_exit,
};

