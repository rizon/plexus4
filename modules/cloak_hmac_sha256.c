/*
 * ircd-hybrid: an advanced Internet Relay Chat Daemon (ircd).
 *
 *  Copyright (c) 2017 Adam <Adam@anope.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at you option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILILTY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have recieved a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 */

#include "stdinc.h"
#include "client.h"
#include "irc_string.h"
#include "ircd.h"
#include "ircd_defs.h"
#include "memory.h"
#include "cloak.h"
#include "conf.h"
#include "log.h"
#include "modules.h"

#include <openssl/hmac.h>

#define CLOAK_PART_LEN 8

static inline const char *
key(struct cloak_config *config)
{
  return config->cloak_key != NULL ? config->cloak_key : cloak_key;
}

static void
hmac(const char *key, const char *data, unsigned char out[SHA256_DIGEST_LENGTH])
{
  HMAC(EVP_sha256(), key, strlen(key), (const unsigned char *) data, strlen(data), out, NULL);
}

static void
base32(unsigned char hash[SHA256_DIGEST_LENGTH], unsigned char out[CLOAK_PART_LEN + 1])
{
  static const unsigned char base32[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";

  assert(CLOAK_PART_LEN <= SHA256_DIGEST_LENGTH);

  for (int i = 0; i < CLOAK_PART_LEN; ++i)
    out[i] = base32[hash[i] & 0x1F];

  out[CLOAK_PART_LEN] = 0;
}

static const char *
hidehost_ipv4(struct cloak_config *config, const char *host)
{
  unsigned int a, b, c, d;
  unsigned char alpha_hash[SHA256_DIGEST_LENGTH], beta_hash[SHA256_DIGEST_LENGTH], gamma_hash[SHA256_DIGEST_LENGTH];
  unsigned char alpha_base32[CLOAK_PART_LEN + 1], beta_base32[CLOAK_PART_LEN + 1], gamma_base32[CLOAK_PART_LEN + 1];
  char buf[IRCD_BUFSIZE];
  static char result[IRCD_BUFSIZE];

  a = b = c = d = 0;

  /*
   * Output: ALPHA.BETA.GAMMA.IP
   * ALPHA is unique for a.b.c.d
   * BETA  is unique for a.b.c.*
   * GAMMA is unique for a.b.*
   * We cloak like this:
   * ALPHA = base32(hmac_sha256(key, a.b.c.d))
   * BETA  = base32(hmac_sha256(key, a.b.c))
   * GAMMA = base32(hmac_sha256(key, a.b))
   */
  sscanf(host, "%u.%u.%u.%u", &a, &b, &c, &d);

  hmac(key(config), host, alpha_hash);
  base32(alpha_hash, alpha_base32);

  snprintf(buf, sizeof(buf), "%u.%u.%u", a, b, c);
  hmac(key(config), buf, beta_hash);
  base32(beta_hash, beta_base32);

  snprintf(buf, sizeof(buf), "%u.%u", a, b);
  hmac(key(config), buf, gamma_hash);
  base32(gamma_hash, gamma_base32);

  snprintf(result, sizeof(result), "%s.%s.%s.IP", alpha_base32, beta_base32, gamma_base32);
  return result;
}

static int
parse_ipv6(const char *host, unsigned int ip[8])
{
  struct addrinfo hints, *res;

  memset(&hints, 0, sizeof(hints));

  hints.ai_family   = AF_INET6;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags    = AI_PASSIVE | AI_NUMERICHOST;

  if (getaddrinfo(host, NULL, &hints, &res))
  {
    return -1;
  }

  assert(res != NULL);

  if (res->ai_family != AF_INET6)
  {
    freeaddrinfo(res);
    return -1;
  }

  struct sockaddr_in6 *in6 = (struct sockaddr_in6 *) res->ai_addr;
  uint16_t *addr = (uint16_t *) &in6->sin6_addr.s6_addr;

  for (int i = 0; i < 8; ++i)
    ip[i] = ntohs(addr[i]);

  freeaddrinfo(res);

  return 0;
}

static const char *
hidehost_ipv6(struct cloak_config *config, const char *host)
{
  unsigned int ip[8];
  unsigned int a = 0, b = 0, c = 0, d = 0, e = 0, f = 0, g = 0, h = 0;
  unsigned char alpha_hash[SHA256_DIGEST_LENGTH], beta_hash[SHA256_DIGEST_LENGTH], gamma_hash[SHA256_DIGEST_LENGTH];
  unsigned char alpha_base32[CLOAK_PART_LEN + 1], beta_base32[CLOAK_PART_LEN + 1], gamma_base32[CLOAK_PART_LEN + 1];
  char buf[IRCD_BUFSIZE];
  static char result[IRCD_BUFSIZE];

  /*
   * Output: ALPHA:BETA:GAMMA:IP
   * ALPHA is unique for a:b:c:d:e:f:g:h
   * BETA  is unique for a:b:c:d:e:f:g
   * GAMMA is unique for a:b:c:d
   * We cloak like this:
   * ALPHA = base32(hmac_sha256(key, a:b:c:d:e:f:g:h))
   * BETA  = base32(hmac_sha256(key, a:b:c:d:e:f:g))
   * GAMMA = base32(hmac_sha256(key, a:b:c:d))
   */

  if (parse_ipv6(host, ip) == 0)
  {
    a = ip[0], b = ip[1], c = ip[2], d = ip[3],
    e = ip[4], f = ip[5], g = ip[6], h = ip[7];
  }

  snprintf(buf, sizeof(buf), "%x:%x:%x:%x:%x:%x:%x:%x", a, b, c, d, e, f, g, h);
  hmac(key(config), buf, alpha_hash);
  base32(alpha_hash, alpha_base32);

  snprintf(buf, sizeof(buf), "%x:%x:%x:%x:%x:%x:%x", a, b, c, d, e, f, g);
  hmac(key(config), buf, beta_hash);
  base32(beta_hash, beta_base32);

  snprintf(buf, sizeof(buf), "%x:%x:%x:%x", a, b, c, d);
  hmac(key(config), buf, gamma_hash);
  base32(gamma_hash, gamma_base32);

  snprintf(result, sizeof(result), "%s:%s:%s:IP", alpha_base32, beta_base32, gamma_base32);
  return result;
}

static const char *
hidehost_normalhost(struct cloak_config *config, const char *host)
{
  unsigned char host_hash[SHA256_DIGEST_LENGTH];
  unsigned char host_base32[CLOAK_PART_LEN + 1];
  const char *p;
  static char result[IRCD_BUFSIZE];

  hmac(key(config), host, host_hash);
  base32(host_hash, host_base32);

  for(p = host; *p; p++)
    if(*p == '.')
      if(isalpha(*(p + 1)))
        break;

  if(*p)
  {
    unsigned int len;
    p++;

    snprintf(result, sizeof(result), "%s-%s.", ServerInfo.network_name, host_base32);
    len = strlen(result) + strlen(p);
    if(len <= HOSTLEN)
      strlcat(result, p, sizeof(result));
    else
      strlcat(result, p + (len - HOSTLEN), sizeof(result));
  }
  else
  {
    snprintf(result, sizeof(result), "%s-%s", ServerInfo.network_name, host_base32);
  }

  return result;
}

static const char *
hidehost(struct cloak_config *config, const char *host)
{
  const char *p;

  /* IPv6 ? */
  if(strchr(host, ':'))
    return hidehost_ipv6(config, host);

  /* Is this a IPv4 IP? */
  for(p = host; *p; p++)
    if(!isdigit(*p) && !(*p == '.'))
      break;
  if(!(*p))
    return hidehost_ipv4(config, host);

  /* Normal host */
  return hidehost_normalhost(config, host);
}

static int
cloak(struct cloak_config *config, const char *curr, char *new, size_t sz)
{
  char host[IRCD_BUFSIZE], *q;
  const char *mask, *p;

  if (key(config) == NULL || !curr || !new)
    return -1;

  /* Convert host to lowercase and cut off at 255 bytes just to be sure */
  for(p = curr, q = host; *p && (q < host + sizeof(host) - 1); p++, q++)
    *q = tolower(*p);
  *q = '\0';

  mask = hidehost(config, host);

  strlcpy(new, mask, sz);
  return 0;
}

static struct CloakSystem cloak_hmac_sha256 = {
  .name = "HMAC-SHA256",
  .cloak = cloak
};

static void
module_init(void)
{
  cloak_register(&cloak_hmac_sha256);
}

static void
module_exit(void)
{
  cloak_unregister(&cloak_hmac_sha256);
}

struct module module_entry =
{
  .version = "$Revision$",
  .modinit = module_init,
  .modexit = module_exit,
};
