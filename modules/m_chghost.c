/*
 *  ircd-hybrid: an advanced Internet Relay Chat Daemon(ircd).
 *  m_chghost.c: Allows changing hostname of connected clients.
 *
 *  Copyright (C) 2002 by the past and present ircd coders, and others.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 *
 *  $Id$
 */

#include "stdinc.h"
#include "client.h"
#include "channel.h"
#include "ircd.h"
#include "irc_string.h"
#include "numeric.h"
#include "fdlist.h"
#include "s_bsd.h"
#include "s_serv.h"
#include "send.h"
#include "parse.h"
#include "modules.h"
#include "s_user.h"
#include "hash.h"
#include "conf.h"
#include "userhost.h"
#include "memory.h"
#include "cloak.h"
#include "dnsbl.h"
#include "channel_mode.h"

static void
me_chghost(struct Client *client_p, struct Client *source_p, int parc, char *parv[])
{
  struct Client *target_p = NULL;

  if(!IsServer(source_p))
    return;

  if ((target_p = find_person(client_p, parv[1])) == NULL)
    return;

  if(strlen(parv[2]) > HOSTLEN || !*parv[2] || !valid_hostname(parv[2]))
    return;

  if(strcmp(target_p->host, parv[2]))
    user_set_hostmask(target_p, NULL, parv[2]);
}

static void
mo_chghost(struct Client *client_p, struct Client *source_p, int parc, char *parv[])
{
  struct Client *target_p = NULL;

  if (!HasUMode(source_p, UMODE_NETADMIN))
  {
    sendto_one(source_p, form_str(ERR_NOPRIVILEGES), me.name, source_p->name);
    return;
  }

  if(parc < 3)
  {
    sendto_one(source_p, form_str(ERR_NEEDMOREPARAMS), me.name, source_p->name, "CHGHOST");
    return;
  }

  target_p = find_person(client_p, parv[1]);
  if (target_p == NULL)
  {
    sendto_one(source_p, form_str(ERR_NOSUCHNICK),
         me.name, source_p->name, parv[1]);
    return;
  }

  if(strlen(parv[2]) > HOSTLEN)
  {
    sendto_one(source_p, form_str(ERR_CANNOTDOCOMMAND),
         me.name, source_p->name, "CHGHOST", "Hostname is too long");
    return;
  }

  if(!*parv[2] || !valid_hostname(parv[2]))
  {
    sendto_one(source_p, form_str(ERR_CANNOTDOCOMMAND),
         me.name, source_p->name, "CHGHOST", "Illegal character in hostname");
    return;
  }

  sendto_server(NULL, CAP_ENCAP, NOCAPS,
          ":%s ENCAP * CHGHOST %s %s",
          me.name, target_p->name, parv[2]);

  if(strcmp(target_p->host, parv[2]))
    user_set_hostmask(target_p, NULL, parv[2]);
}

/* CHGREALHOST target IP host */
static void
me_chgrealhost(struct Client *client_p, struct Client *source_p, int parc, char *parv[])
{
  struct Client *target_p;
  struct addrinfo hints, *res;
  int was_userhost = 0;

  if (!IsServer(source_p))
    return;

  target_p = find_person(client_p, parv[1]);
  if (target_p == NULL)
  {
    /* allow CHGREALHOST on unknown users */
    target_p = hash_find_id(parv[1]);
    if (target_p == NULL || !IsUnknown(target_p))
      return;
  }

  if (!valid_hostname(parv[3]))
    return;

  memset(&hints, 0, sizeof(hints));

  hints.ai_family   = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags    = AI_PASSIVE | AI_NUMERICHOST;

  if (getaddrinfo(parv[2], NULL, &hints, &res))
    return;

  assert(res != NULL);

  if (MyConnect(target_p))
    clear_dnsbl_lookup(target_p);

  if (IsUserHostIp(target_p))
  {
    userhost_del(target_p->realhost, !MyConnect(target_p));
    ClearUserHost(target_p);
    was_userhost = 1;
  }

  if (MyConnect(target_p))
  {
    memcpy(&target_p->localClient->ip, res->ai_addr, res->ai_addrlen);
    target_p->localClient->ip.ss_len = res->ai_addrlen;
    target_p->localClient->ip.ss.ss_family = res->ai_family;
    target_p->localClient->aftype = res->ai_family;
  }

  freeaddrinfo(res);

  if (target_p->cgisockhost == NULL)
    target_p->cgisockhost = xstrdup(target_p->sockhost);
  if (target_p->cgihost == NULL)
    target_p->cgihost = xstrdup(target_p->realhost);

  strlcpy(target_p->sockhost, parv[2], sizeof(target_p->sockhost));
  strlcpy(target_p->realhost, parv[3], sizeof(target_p->realhost));

  if (IsClient(target_p))
  {
    if (MyConnect(target_p))
    {
      clear_ban_cache_client(target_p);

      if (!IsIPSpoof(target_p))
      {
        cloak_client(&target_p->localClient->cloaks, target_p->realhost, target_p->sockhost);
      }
    }

    if (was_userhost)
    {
      userhost_add(target_p->realhost, !MyConnect(target_p));
      SetUserHost(target_p);
    }
  }
  else if (IsUnknown(target_p))
  {
    strlcpy(target_p->host, target_p->realhost, sizeof(target_p->host));
  }

  if (MyConnect(target_p))
  {
    check_klines(target_p);
    if (IsDefunct(target_p))
      return;

    start_dnsbl_lookup(target_p);
  }
}

static struct Message chghost_msgtab = {
  .cmd = "CHGHOST",
  .args_min = 3,
  .args_max = MAXPARA,
  .handlers[UNREGISTERED_HANDLER] = m_unregistered,
  .handlers[CLIENT_HANDLER] = m_ignore,
  .handlers[SERVER_HANDLER] = m_ignore,
  .handlers[ENCAP_HANDLER] = me_chghost,
  .handlers[OPER_HANDLER] = mo_chghost,
};

static struct Message chgrealhost_msgtab = {
  .cmd = "CHGREALHOST",
  .args_min = 4,
  .args_max = MAXPARA,
  .handlers[UNREGISTERED_HANDLER] = m_unregistered,
  .handlers[CLIENT_HANDLER] = m_ignore,
  .handlers[SERVER_HANDLER] = m_ignore,
  .handlers[ENCAP_HANDLER] = me_chgrealhost,
  .handlers[OPER_HANDLER] = m_ignore,
};

static void
module_init(void)
{
  mod_add_cmd(&chghost_msgtab);
  mod_add_cmd(&chgrealhost_msgtab);
}

static void
module_exit(void)
{
  mod_del_cmd(&chghost_msgtab);
  mod_del_cmd(&chgrealhost_msgtab);
}

struct module module_entry = {
  .version = "$Revision$",
  .modinit = module_init,
  .modexit = module_exit,
};

