/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2015-2016 plexus development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "stdinc.h"
#include "client.h"
#include "irc_string.h"
#include "numeric.h"
#include "modules.h"
#include "memory.h"
#include "hook.h"
#include "extban.h"
#include "channel_mode.h"

static struct Extban extban_mute;

static void
extban_mute_can_send(struct can_send_data *data)
{
  struct Client *client = data->client;
  struct Channel *chptr = data->chptr;
  struct Membership *ms = data->membership;

  if (!MyClient(client))
    return;

  if (ms)
  {
    if (ms->flags & CHFL_BAN_SILENCED)
    {
      data->ret = ERR_CANNOTSENDTOCHAN;
      return;
    }

    if (ms->flags & CHFL_MUTE_CHECKED)
      return;

    ms->flags |= CHFL_MUTE_CHECKED;
  }

  /* search for matching muteban */
  if (find_bmask(client, &chptr->banlist, &extban_mute))
    /* clients who match +e m: override +b m: */
    if (!find_bmask(client, &chptr->exceptlist, &extban_mute))
    {
      if (ms)
        ms->flags |= CHFL_BAN_SILENCED;

      data->ret = ERR_CANNOTSENDTOCHAN;
    }
}

static struct Extban extban_mute =
{
  .character = 'm',
  .type = EXTBAN_ACTING,
  .types = CHFL_BAN | CHFL_EXCEPTION
};

static struct Event can_send_event =
{
  .event = &can_send_hook,
  .handler = extban_mute_can_send
};

static void
module_init(void)
{
  extban_add(&extban_mute);
  hook_add(&can_send_event);
}

static void
module_exit(void)
{
  extban_del(&extban_mute);
  hook_del(&can_send_event);
}

struct module module_entry =
{
  .version = "$Revision$",
  .modinit = module_init,
  .modexit = module_exit,
};
