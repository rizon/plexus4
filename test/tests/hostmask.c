/*
 *  ircd-hybrid: an advanced Internet Relay Chat Daemon(ircd).
 *
 *  Copyright (C) 2019 Adam <Adam@anope.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "plexus_test.h"

START_TEST(hostmask_test)
{
  ck_assert_int_eq(parse_netmask("0000:0000:0000:0000:0000:0000:0000:0001/1", NULL, NULL), HM_IPV6);
  ck_assert_int_eq(parse_netmask("0000:0000:0000:0000:0000:0000:0000:1000/127", NULL, NULL), HM_IPV6);
}
END_TEST

void
hostmask_setup(Suite *s)
{
  TCase *tc = tcase_create("hostmask");

  tcase_add_test(tc, hostmask_test);

  suite_add_tcase(s, tc);
}
