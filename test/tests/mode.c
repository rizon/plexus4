/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2016 plexus development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "plexus_test.h"

static void
umode_expect(struct PlexusClient *c, const char *modes, int numeric)
{
  irc_client_set_umode(c, modes);

  if (numeric)
    expect_numeric(c, numeric);
  else
    expect_message_args(c, c->client, "MODE %s :%s", c->name, modes);
}

static void
snomask_expect(struct PlexusClient *c, const char *snomask)
{
  irc_client_set_umode(c, snomask);

  expect_message_args(c, c->client, "MODE %s :+s", c->name);
  expect_numeric(c, RPL_SNOMASK);
}

static void
cmode_expect(struct PlexusClient *c, struct Channel *chptr, const char *modes, int numeric)
{
  irc_client_set_cmode(c, chptr, modes);
  if (numeric)
    expect_numeric(c, numeric);
  else
    expect_message(c, c->client, "MODE");
}

static void
join_no_part(struct PlexusClient *c)
{
  irc_join(c, "#a");
  expect_message(c, c->client, "JOIN");
}

static void
join_no_part_chan(struct PlexusClient *c, const char *chan)
{
  irc_join(c, chan);
  expect_message(c, c->client, "JOIN");
}

static void
join_cycle(struct PlexusClient *c, int numeric)
{
  irc_join(c, "#a");
  if (numeric)
    expect_numeric(c, numeric);
  else
  {
    expect_message(c, c->client, "JOIN");
    irc_part(c, "#a");
    expect_message(c, c->client, "PART");
  }
}

static void
privmsg_expect(struct PlexusClient *c, const char *target, const char *message, int numeric)
{
  irc_privmsg(c, target, message);
  if (numeric)
    expect_numeric(c, numeric);
}

static void
part_routine(struct PlexusClient *c)
{
  irc_part(c, "#a");
  expect_message(c, c->client, "PART");
}

static void
part_routine_chan(struct PlexusClient *c, const char *chan)
{
  irc_part(c, chan);
  expect_message(c, c->client, "PART");
}

START_TEST(umode)
{
  plexus_up();

  struct PlexusClient *client1 = client_register("test1"),
                      *client2 = client_register("test2");

  SetCanFlood(client1->client);
  SetCanFlood(client2->client);

  ck_assert(client1->client->flags & FLAGS_CANFLOOD);
  ck_assert(client2->client->flags & FLAGS_CANFLOOD);

  ++Count.oper;
  SetOper(client1->client);

  ck_assert(client1->client->umodes & UMODE_OPER);

  umode_expect(client1, "+C", 0);
  umode_expect(client1, "+D", 0);
  umode_expect(client1, "+G", 0);
  umode_expect(client1, "+R", 0);
  umode_expect(client1, "+g", 0);
  umode_expect(client1, "+p", 0);
  umode_expect(client1, "+w", 0);

  // No need to write a "umode on others" function, it's a one time test
  io_write(client1, "MODE %s +D", client2->client->name);
  expect_numeric(client1, ERR_USERSDONTMATCH);

  umode_expect(client1, "+A", ERR_UMODEUNKNOWNFLAG);
  snomask_expect(client1, "+s +c");

  ck_assert_int_eq(client1->client->umodes, (UMODE_CLOAK |
                   UMODE_DEAF | UMODE_SERVNOTICE | UMODE_INVISIBLE | UMODE_OPER |
                   UMODE_NOCTCP | UMODE_SOFTCALLERID | UMODE_REGONLY | UMODE_CALLERID |
                   UMODE_HIDECHANNELS | UMODE_WALLOP));

  ck_assert_int_eq(client2->client->umodes, UMODE_CLOAK | UMODE_INVISIBLE);
}
END_TEST

START_TEST(cmode)
{
  plexus_up();

  struct PlexusClient *client1 = client_register("test1"),
                      *client2 = client_register("test2");

  SetCanFlood(client1->client);
  SetCanFlood(client2->client);

  client1->client->flags  |= FLAGS_SSL;
  client1->client->umodes |= UMODE_SSL;

  ck_assert(client1->client->flags & (FLAGS_CANFLOOD|FLAGS_SSL));
  ck_assert(client2->client->flags & FLAGS_CANFLOOD);

  ++Count.oper;
  SetOper(client1->client);

  ck_assert(client1->client->umodes & UMODE_OPER);

  join_no_part(client1);

  struct Channel *chptr = hash_find_channel("#a");
  ck_assert_ptr_ne(chptr, NULL);

  struct Membership *client1_ms = NULL, *client2_ms = NULL;
  ck_assert_ptr_ne((client1_ms = find_channel_link(client1->client, chptr)), NULL);

  // Make sure we got op'd
  ck_assert(has_member_flags(client1_ms, CHFL_CHANOP));

  // Default +nt
  ck_assert_int_eq(chptr->mode.mode, (MODE_TOPICLIMIT|MODE_NOPRIVMSGS));

  cmode_expect(client1, chptr, "+z", ERR_NOPRIVS);
  cmode_expect(client1, chptr, "+O", 0);
  join_cycle(client2, ERR_OPERONLYCHAN);
  cmode_expect(client1, chptr, "-O", 0);

  join_no_part(client2);
  cmode_expect(client2, chptr, "+O", ERR_CHANOPRIVSNEEDED);

  // Force the ircd to give us owner
  AddMemberFlag(client1_ms, CHFL_OWNER);
  ck_assert_int_eq(client1_ms->flags, (CHFL_CHANOP|CHFL_OWNER));

  cmode_expect(client1, chptr, "+o test2", 0);
  ck_assert_ptr_ne((client2_ms = find_channel_link(client2->client, chptr)), NULL);
  ck_assert(client2_ms->flags & CHFL_CHANOP);
  cmode_expect(client2, chptr, "+O", ERR_NOPRIVILEGES);
  cmode_expect(client2, chptr, "+z", ERR_NOPRIVS);
  cmode_expect(client1, chptr, "+S", ERR_CANNOTCHANGECHANMODE);
  cmode_expect(client1, chptr, "+lk 24 testkey", 0);
  ck_assert_int_eq(chptr->mode.limit, 24);
  ck_assert(chptr->mode.key[0]);
  ck_assert_str_eq(chptr->mode.key, "testkey");

  part_routine(client2);
  cmode_expect(client1, chptr, "+O", 0);
  cmode_expect(client1, chptr, "-O", 0);
  cmode_expect(client2, chptr, "+t", ERR_NOTONCHANNEL);
  cmode_expect(client1, chptr, "+o test2", ERR_USERNOTINCHANNEL);
  cmode_expect(client1, chptr, "+o noone", ERR_NOSUCHNICK);

  ck_assert_int_eq(chptr->mode.mode, (MODE_TOPICLIMIT|MODE_NOPRIVMSGS));
  cmode_expect(client1, chptr, "-tn", 0);
  ck_assert_int_eq(chptr->mode.mode, 0);
  part_routine(client1);

  ck_assert_ptr_eq((hash_find_channel("#a")), NULL);

  join_no_part(client1);
  ck_assert_ptr_ne((chptr = hash_find_channel("#a")), NULL);
  ck_assert_ptr_ne((client1_ms = find_channel_link(client1->client, chptr)), NULL);
  ck_assert(has_member_flags(client1_ms, CHFL_CHANOP));
  chptr->mode.mode |= MODE_PERSIST; // Ensure the channel does not delete
  cmode_expect(client1, chptr, "+itnslIkS 14 test1!*@* testkey", 0);
  cmode_expect(client1, chptr, "+bbeI *!*@ban.one *!*@ban.two *!*@banex.one *!*@invex.one", 0);
  part_routine(client1);
  ck_assert_ptr_eq(hash_find_channel("#a"), chptr);
  join_cycle(client1, ERR_BADCHANNELKEY);
  irc_join_key(client1, "#a", "testkey");
  expect_message(client1, client1->client, "JOIN");
  // This is not an SJOIN, we should NOT be op'd
  ck_assert_ptr_ne(find_channel_link(client1->client, chptr), NULL);
  ck_assert(!has_member_flags(client1_ms, CHFL_CHANOP));
  // What should be there
  ck_assert(chptr->mode.mode &
    (MODE_INVITEONLY|MODE_TOPICLIMIT|MODE_NOPRIVMSGS|MODE_SECRET|MODE_PERSIST|MODE_SSLONLY));
  ck_assert_int_eq(chptr->mode.limit, 14);
  ck_assert_str_eq(chptr->mode.key, "testkey");
  ck_assert_int_eq(dlink_list_length(&chptr->banlist), 2);
  ck_assert_int_eq(dlink_list_length(&chptr->exceptlist), 1);
  ck_assert_int_eq(dlink_list_length(&chptr->invexlist), 2);
}
END_TEST

START_TEST(cmode_bmask)
{
  plexus_up();

  // Introduce some clients
  struct PlexusClient *client1 = client_register("test1"),
                      *client2 = client_register("test2"),
                      *client3 = client_register_host("test3", "192.168.23.14"),
                      *client4 = client_register_host("test4", "fd3f:587b:b82f:f3d1:32df:abcd:fd32:d3ad"),
                      *client5 = client_register_host("test5", "2606:6000:670b:ae00:c87f:82f:b106:205b"),
                      *client6 = client_register_host("test6", "183.94.200.53"),
                      *client7 = client_register_host("test7", "181.254.200.19"),
                      *client8 = client_register_host("test8", "2001:321f:23dc::babe");

  // Give them CAN_FLOOD
  SetCanFlood(client1->client);
  SetCanFlood(client2->client);
  SetCanFlood(client3->client);
  SetCanFlood(client4->client);
  SetCanFlood(client5->client);
  SetCanFlood(client6->client);
  SetCanFlood(client7->client);
  SetCanFlood(client8->client);

  // Setup some clients with specific data
  client2->client->umodes |= UMODE_REGISTERED;
  strlcpy(client2->client->suser, "badacct", sizeof(client2->client->suser));
  strlcpy(client2->client->info, "gecoext", sizeof(client2->client->info));
  client2->client->certfp = xstrdup("0759cd50ccdfa128cc07c4769dddd8ac362b393d");
  strlcpy(client7->client->host, "my.awesome.ircd", sizeof(client7->client->host));
  strlcpy(client8->client->host, "how.do.i.vhost.", sizeof(client8->client->host));
  AddUMode(client7->client, UMODE_CLOAK);
  AddUMode(client8->client, UMODE_CLOAK);

  // This seems redundant, but the theory is tests should
  // test the impossible
  ck_assert(client1->client->flags & FLAGS_CANFLOOD);
  ck_assert(client2->client->flags & FLAGS_CANFLOOD);
  ck_assert(client3->client->flags & FLAGS_CANFLOOD);
  ck_assert(client4->client->flags & FLAGS_CANFLOOD);
  ck_assert(client5->client->flags & FLAGS_CANFLOOD);
  ck_assert(client6->client->flags & FLAGS_CANFLOOD);
  ck_assert(client7->client->flags & FLAGS_CANFLOOD);
  ck_assert(client8->client->flags & FLAGS_CANFLOOD);

  // Create #a
  join_no_part(client1);

  // Create the pointer to #a, setup pointer to #b for later
  struct Channel *chptr = hash_find_channel("#a"),
                 *chptrb = NULL;
  ck_assert_ptr_ne(chptr, NULL);

  // A lot of CIDR abuse, most of the comments are to the side of
  // each action.
  cmode_expect(client1, chptr, "+bbbbb *!*@* *!*@123.123.123.123 *!*@192.168.0.0/16 *!*@fd3f:587b:b82f::/48 *!*@fd4f:6b19:f094::/48", 0);
  ck_assert_int_eq(dlink_list_length(&chptr->banlist), 4);
  cmode_expect(client1, chptr, "-b *!*@*", 0);
  ck_assert_int_eq(dlink_list_length(&chptr->banlist), 3);
  cmode_expect(client1, chptr, "+b *!*@2606:6000:670b:ae00::/56", 0);
  ck_assert_int_eq(dlink_list_length(&chptr->banlist), 4);
  join_cycle(client3, ERR_BANNEDFROMCHAN); // Hits the v4 /16 ban
  join_cycle(client4, ERR_BANNEDFROMCHAN); // Hits the v6 /48 ban
  join_cycle(client5, ERR_BANNEDFROMCHAN); // Hits the v6 /56 ban
  join_cycle(client2, 0);
  cmode_expect(client1, chptr, "+b *!*@183.94.192.0/18", 0);
  join_cycle(client6, ERR_BANNEDFROMCHAN); // Hits the v4 /18
  cmode_expect(client1, chptr, "-b+b *!*@183.94.192.0/18 *!*@183.94.192.0/23", 0);
  join_cycle(client6, 0); // Misses the broken /23
  cmode_expect(client1, chptr, "-b+be *!*@183.94.192.0/23 *!*@2606:6000:670b:a000::/52 *!*@2606:6000:670b::/48", 0);
  ck_assert_int_eq(dlink_list_length(&chptr->exceptlist), 1);
  join_cycle(client5, 0); // Joins through the +b /52 from the +e /48
  cmode_expect(client1, chptr, "-be+iI *!*@2606:6000:670b:a000::/52 *!*@2606:6000:670b::/48 *!*@183.94.192.0/18", 0);
  join_cycle(client6, 0); // Joins through the +i from the +I /18
  ck_assert_int_eq(dlink_list_length(&chptr->banlist), 4);
  ck_assert_int_eq(dlink_list_length(&chptr->invexlist), 1);
  cmode_expect(client6, chptr, "+b *!*@*", ERR_NOTONCHANNEL);
  cmode_expect(client1, chptr, "+bbbeI *!*@* *!*@ban.one *!*@ban.two *!*@banex.one *!*@invex.one", 0);
  cmode_expect(client1, chptr, "-i", 0);
  ck_assert_int_eq(dlink_list_length(&chptr->banlist), 7);
  ck_assert_int_eq(dlink_list_length(&chptr->exceptlist), 1);
  ck_assert_int_eq(dlink_list_length(&chptr->invexlist), 1);
  join_cycle(client2, ERR_BANNEDFROMCHAN); // Banned from *!*@*
  cmode_expect(client1, chptr, "-bbbe *!*@* *!*@ban.one *!*@ban.two *!*@banex.one", 0);
  ck_assert_int_eq(dlink_list_length(&chptr->banlist), 4);
  ck_assert_int_eq(dlink_list_length(&chptr->exceptlist), 0);
  ck_assert_int_eq(dlink_list_length(&chptr->invexlist), 1);
  cmode_expect(client1, chptr, "+bbb a:badacct a:somerandomaccount a:morerandom", 0);
  join_cycle(client2, ERR_BANNEDFROMCHAN);
  cmode_expect(client1, chptr, "+e *!*@127.0.0.0/8", 0);
  join_cycle(client2, 0); // Joins through the +b a:badacct from the +e /8
  cmode_expect(client1, chptr, "-bbb+b a:badacct a:somerandomaccount a:morerandom r:gecoext", 0);
  join_cycle(client2, 0); // Joins through the +b r:gecoext from the +e /8
  cmode_expect(client1, chptr, "-e *!*@127.0.0.0/8", 0);
  join_cycle(client2, ERR_BANNEDFROMCHAN); // Banned from +b r:gecoext
  cmode_expect(client1, chptr, "-b+b r:gecoext a:badacct", 0);
  join_cycle(client2, ERR_BANNEDFROMCHAN); // Banned from +b a:badacct
  cmode_expect(client1, chptr, "-b+b a:badacct m:*!*@how.do.i*", 0);
  join_no_part(client8);
  privmsg_expect(client8, "#a", "blah", ERR_CANNOTSENDTOCHAN); // Silenced
  cmode_expect(client1, chptr, "+e *!*@how.do.i.vhost", 0);
  privmsg_expect(client8, "#a", "blah", ERR_CANNOTSENDTOCHAN); // Silenced, not a +e m:
  cmode_expect(client1, chptr, "+e m:*!*@how.do.i.vhost", 0);
  privmsg_expect(client8, "#a", "blah", 0); // Message through +e m:
  privmsg_expect(client8, "#a", "blah", 0);
  privmsg_expect(client8, "#a", "blah", 0);
  cmode_expect(client1, chptr, "-eeb *!*@how.do.i.vhost m:*!*@how.do.i.vhost m:*!*@how.do.i*", 0);
  cmode_expect(client1, chptr, "+b", RPL_BANLIST); // Send the ban list
  cmode_expect(client1, chptr, "+I", RPL_INVITELIST);
  cmode_expect(client1, chptr, "+e excepttest!*@*", 0);
  cmode_expect(client1, chptr, "+e", RPL_EXCEPTLIST);
  ck_assert_int_eq(dlink_list_length(&chptr->banlist), 4);
  ck_assert_int_eq(dlink_list_length(&chptr->exceptlist), 1);
  ck_assert_int_eq(dlink_list_length(&chptr->invexlist), 1);
  cmode_expect(client1, chptr, "-bbbb *!*@123.123.123.123 *!*@192.168.0.0/16 *!*@fd3f:587b:b82f::/48 *!*@2606:6000:670b:ae00::/56", 0);
  cmode_expect(client1, chptr, "-Ie *!*@183.94.192.0/18 excepttest!*@*", 0);
  ck_assert_int_eq(dlink_list_length(&chptr->banlist), 0);
  ck_assert_int_eq(dlink_list_length(&chptr->invexlist), 0);
  ck_assert_int_eq(dlink_list_length(&chptr->exceptlist), 0);
  cmode_expect(client1, chptr, "+be *!*@my.kawaii.senapi *!*@181.254.128.0/17", 0);
  join_no_part(client7); // Joins through the +b vhost from the +e /17
  cmode_expect(client1, chptr, "+be m:*!*@181.254.128.0/17 m:*!*@my*", 0);
  privmsg_expect(client7, "#a", "blah", 0); // Messages through the /17 from the wildcard vhost
  cmode_expect(client1, chptr, "-e m:*!*@my*", 0);
  privmsg_expect(client7, "#a", "blah", ERR_CANNOTSENDTOCHAN);
  part_routine(client1);
  part_routine(client7);
  part_routine(client8);
  ck_assert_ptr_eq(hash_find_channel("#a"), NULL);
  join_no_part(client1);
  ck_assert_ptr_ne((chptr = hash_find_channel("#a")), NULL);

  // Random/nonsense bans
  cmode_expect(client1, chptr, "+b *!*@moo/peer/32/-20000", 0);
  join_cycle(client3, 0);
  cmode_expect(client1, chptr, "+b *!*@192.168.23.0/-43", 0);
  join_cycle(client3, 0);
  cmode_expect(client1, chptr, "+b *!*@192.168.23.0/5000", 0);
  join_cycle(client3, 0);
  cmode_expect(client1, chptr, "+b *!*@192.0.0.0/-3000000000", 0);
  join_cycle(client3, 0);
  cmode_expect(client1, chptr, "+b *!*@-435434543543453/*!*@*", 0);
  join_cycle(client3, 0);
  cmode_expect(client1, chptr, "+b ########!*@*", 0);
  join_cycle(client3, 0);
  cmode_expect(client1, chptr, "+b *!*@rizon/staff/lol", 0);
  join_cycle(client3, 0);
  cmode_expect(client1, chptr, "+b *!*@/!!@##", 0);
  join_cycle(client3, 0);
  cmode_expect(client1, chptr, "+b *!*@/-21000000000", 0);
  join_cycle(client3, 0);
  cmode_expect(client1, chptr, "+b *!*@127.0.0.0/-21000000000", 0);
  join_cycle(client3, 0);
  cmode_expect(client1, chptr, "+b *!*@$#cake", 0);
  join_cycle(client3, 0);
  cmode_expect(client1, chptr, "+b ******!******@**FOOM******", 0);
  join_cycle(client3, 0);
  cmode_expect(client1, chptr, "+b ***********!*********@******.168.********", 0);
  join_cycle(client3, ERR_BANNEDFROMCHAN); // Got translated to *!*@*.168.*
  ck_assert_int_eq(dlink_list_length(&chptr->banlist), 13);
  cmode_expect(client1, chptr, "-bbbb *!*@moo/peer/32/-20000 *!*@192.168.23.0/-43 *!*@192.168.23.0/5000 *!*@192.0.0.0/-3000000000", 0);
  cmode_expect(client1, chptr, "-bbbb *!*@-435434543543453/*!*@* ########!*@* *!*@rizon/staff/lol *!*@/!!@##", 0);
  cmode_expect(client1, chptr, "-bbbb *!*@/-21000000000 *!*@127.0.0.0/-21000000000 *!*@$#cake *!*@*FOOM*", 0);
  cmode_expect(client1, chptr, "-b *!*@*.168.*", 0);
  ck_assert_int_eq(dlink_list_length(&chptr->banlist), 0);

  // #b will be our forward test channel
  join_no_part_chan(client1, "#b");
  ck_assert_ptr_ne((chptrb = hash_find_channel("#b")), NULL);

  // forwards can only be set through +e
  cmode_expect(client1, chptrb, "+b f:#a", ERR_INVALIDBAN);
  cmode_expect(client1, chptrb, "+I f:#a", ERR_INVALIDBAN);
  cmode_expect(client1, chptrb, "+e f:#a", 0);
  join_cycle(client7, 0); // Join/parting #a

  // client7 will now be forwarded to #b
  cmode_expect(client1, chptr, "+b test7!*@*#b", 0);
  irc_join(client7, "#a");
  expect_numeric(client7, ERR_LINKCHANNEL);
  ck_assert_int_eq(dlink_list_length(&chptr->members), 1);
  ck_assert_int_eq(dlink_list_length(&chptrb->members), 2);
  part_routine_chan(client7, "#b");

  // client7 should now just get a ban message for #a
  cmode_expect(client1, chptr, "-b+b test7!*@*#b test7!*@*", 0);
  join_cycle(client7, ERR_BANNEDFROMCHAN); // Banned from #a
  join_no_part_chan(client7, "#b"); // But not from #b
  part_routine_chan(client7, "#b");

  // client2 will be forwarded from #a to #b but will be banned from #b
  cmode_expect(client1, chptr, "-b+b test7!*@* test2!*@*#b", 0);
  cmode_expect(client1, chptrb, "+b z:0759cd50ccdfa128cc07c4769dddd8ac362b393d", 0);
  irc_join(client2, "#a");
  expect_numeric(client2, ERR_LINKCHANNEL);
  expect_numeric(client2, ERR_BANNEDFROMCHAN);
  ck_assert_int_eq(dlink_list_length(&chptr->members), 1);
  ck_assert_int_eq(dlink_list_length(&chptrb->members), 1);

  // Cleanup
  part_routine(client1);
  part_routine_chan(client1, "#b");
  ck_assert_ptr_eq(hash_find_channel("#a"), NULL);
  ck_assert_ptr_eq(hash_find_channel("#b"), NULL);
}
END_TEST

START_TEST(cmode_bmask2)
{
  plexus_up();

  // Introduce some clients
  struct PlexusClient *client1 = client_register("test1");

  join_no_part(client1);

  struct Channel *chptr = hash_find_channel("#a");
  ck_assert_ptr_ne(chptr, NULL);

  cmode_expect(client1, chptr, "+bbb a!*@* b!*@* c!*@*", 0);
  cmode_expect(client1, chptr, "-bbb z!*@* b!*@* c!*@*", 0);

  ck_assert_int_eq(chptr->banlist.length, 1);
}
END_TEST

void
mode_setup(Suite *s)
{
  TCase *tc = tcase_create("mode");

  tcase_add_checked_fixture(tc, NULL, plexus_down);
  tcase_add_test(tc, umode);
  tcase_add_test(tc, cmode);
  tcase_add_test(tc, cmode_bmask);
  tcase_add_test(tc, cmode_bmask2);

  suite_add_tcase(s, tc);
}
