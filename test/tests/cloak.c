/*
 *  ircd-hybrid: an advanced Internet Relay Chat Daemon(ircd).
 *
 *  Copyright (C) 2017 Adam <Adam@anope.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "plexus_test.h"

START_TEST(cloak_test)
{
  struct PlexusClient *client = client_register("test1");

  ck_assert(HasUMode(client->client, UMODE_CLOAK));
  ck_assert(!strcmp(client->client->host, "Rizon-A3D4A04") // localhost
      || !strcmp(client->client->host, "FC78B61F.1D3D3A06.16F35E48.IP") // 127.0.0.1
  );
}
END_TEST

void
cloak_setup(Suite *s)
{
  TCase *tc = tcase_create("cloak");

  tcase_add_checked_fixture(tc, plexus_up, plexus_down);
  tcase_add_test(tc, cloak_test);

  suite_add_tcase(s, tc);
}
