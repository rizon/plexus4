/*
 *  ircd-hybrid: an advanced Internet Relay Chat Daemon(ircd).
 *
 *  Copyright (C) 2017 Adam <Adam@anope.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "plexus_test.h"

static struct MaskItem *
webirc_find(const char *address)
{
  struct irc_ssaddr addr;
  int bits;

  int masktype = parse_netmask(address, &addr, &bits);

  struct MaskLookup lookup = {
    .name     = address,
    .fam      = masktype != HM_HOST ? addr.ss.ss_family : 0,
    .addr     = masktype != HM_HOST ? &addr : NULL,
    .cmpfunc  = irccmp
  };

  return find_conf_by_address(CONF_WLINE, &lookup);
}

static struct MaskItem *
webirc_create(const char *mask, const char *password)
{
  struct MaskItem *conf = conf_make(CONF_WLINE);

  conf->passwd = xstrdup(password);
  conf->user   = xstrdup("Adam");
  conf->host   = xstrdup(mask);
  conf->setat  = CurrentTime;
  conf->until  = CurrentTime + 42L;
  conf->reason = xstrdup("test");

  AddFlag(conf, CONF_FLAGS_NEED_PASSWORD);

  SetConfDatabase(conf);

  add_conf_by_address(CONF_WLINE, conf);

  return conf;
}

START_TEST(webirc_test)
{
  struct PlexusClient *client = client_create("test");

  webirc_create("127.0.0.1", "hunter2");

  io_write(client, "WEBIRC hunter2 . fakehost 1.2.3.4");

  io_write(client, "USER %s . . :%s", client->name, client->name);
  io_write(client, "NICK %s", client->name);

  expect_numeric(client, RPL_WELCOME);

  ck_assert_str_eq(client->client->realhost, "fakehost");
  ck_assert_str_eq(client->client->sockhost, "1.2.3.4");
}
END_TEST

START_TEST(webirc_test_localhost)
{
  struct PlexusClient *client = client_create("test");

  webirc_create("127.0.0.1", "hunter2");

  io_write(client, "WEBIRC hunter2 . fakehost :::1");

  io_write(client, "USER %s . . :%s", client->name, client->name);
  io_write(client, "NICK %s", client->name);

  expect_numeric(client, RPL_WELCOME);

  ck_assert_str_eq(client->client->realhost, "fakehost");
  ck_assert_str_eq(client->client->sockhost, "0::1");
}
END_TEST

START_TEST(swebirc_test)
{
  struct PlexusClient *server = server_register("plexus4.2");

  struct MaskItem *conf = conf_make(CONF_ULINE);
  conf->flags = SHARED_SESSION;
  conf->name = xstrdup(server->name);

  const time_t time = 123456789;

  // set

  io_write(server, "ENCAP %s SWEBIRC SET %s %s %s %lu %lu :%s",
           me.name, "test.mask", "pass", "Adam", time, time, "test webirc");

  expect_pingwait(server, &me);

  struct MaskItem *webirc = webirc_find("test.mask");
  ck_assert_ptr_ne(webirc, NULL);

  ck_assert_str_eq(webirc->user, "Adam");
  ck_assert_str_eq(webirc->host, "test.mask");
  ck_assert_int_eq(webirc->setat, time);
  ck_assert_int_eq(webirc->until, time);
  ck_assert_str_eq(webirc->reason, "test webirc");
  ck_assert(!HasFlag(webirc, CONF_FLAGS_WEBIRC_CLOSED));

  // close

  io_write(server, "ENCAP %s SWEBIRC CLOSE %s",
           me.name, webirc->host);

  expect_pingwait(server, &me);

  ck_assert(HasFlag(webirc, CONF_FLAGS_WEBIRC_CLOSED));

  // open

  io_write(server, "ENCAP %s SWEBIRC OPEN %s",
           me.name, webirc->host);

  expect_pingwait(server, &me);

  ck_assert(!HasFlag(webirc, CONF_FLAGS_WEBIRC_CLOSED));

  // unset

  io_write(server, "ENCAP %s SWEBIRC UNSET %s",
           me.name, webirc->host);

  expect_pingwait(server, &me);

  webirc = webirc_find("test.mask");
  ck_assert_ptr_eq(webirc, NULL);
}
END_TEST

void
webirc_setup(Suite *s)
{
  TCase *tc = tcase_create("webirc");

  tcase_add_checked_fixture(tc, plexus_up, plexus_down);
  tcase_add_test(tc, webirc_test);
  tcase_add_test(tc, webirc_test_localhost);
  tcase_add_test(tc, swebirc_test);

  suite_add_tcase(s, tc);
}
