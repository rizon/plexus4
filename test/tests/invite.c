/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2016 plexus development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "plexus_test.h"

START_TEST(invite)
{
  plexus_up();

  struct PlexusClient *client1 = client_register("test1"),
                      *client2 = client_register("test2");

  irc_join(client1, "#a");
  expect_message(client1, client1->client, "JOIN");

  struct Channel *chptr = hash_find_channel("#a");
  ck_assert_ptr_ne(chptr, NULL);

  chptr->mode.mode |= MODE_INVITEONLY | MODE_PRIVATE;

  irc_invite(client1, chptr, client2->client);
  expect_numeric(client1, RPL_INVITING);

  expect_message(client2, client1->client, "INVITE");

  plexus_down();
}
END_TEST

START_TEST(invite_notify)
{
  plexus_up();

  struct PlexusClient *client1 = client_register("test1"),
                      *client2 = client_register("test2");

  client1->client->localClient->cap_active |= CAP_INVITE_NOTIFY;

  irc_join(client1, "#a");
  expect_message(client1, client1->client, "JOIN");

  struct Channel *chptr = hash_find_channel("#a");
  ck_assert_ptr_ne(chptr, NULL);

  irc_invite(client1, chptr, client2->client);
  expect_numeric(client1, RPL_INVITING);
  expect_message(client1, client1->client, "INVITE"); // invite-notify

  expect_message(client2, client1->client, "INVITE");

  plexus_down();
}
END_TEST

static void
other()
{
  plexus_up_conf(RESOURCEDIR "/test2.conf");

  WAIT_FOR(hash_find_client("plexus4.") != NULL);

  struct PlexusClient *client2 = client_register("test2");

  WAIT_FOR(hash_find_client("test1") != NULL);
  struct Client *client = hash_find_client("test1");

  expect_message(client2, client, "INVITE");

  struct Channel *chptr = hash_find_channel("#a");
  ck_assert_ptr_ne(chptr, NULL);

  ck_assert_int_eq(dlink_list_length(&chptr->invites), 1);

  struct Invite *invite = find_invite(chptr, client2->client);

  ck_assert_ptr_ne(invite, NULL);
  ck_assert_ptr_eq(invite->client, client2->client);
  ck_assert_ptr_eq(invite->chptr, chptr);
  ck_assert_ptr_eq(invite->from, NULL); // from isn't kept from non local clients

  chptr->mode.mode |= MODE_PERSIST; // keep channel from going away when client is exited

  exit_client(client, &me, "forced quit"); // will desync other side

  ck_assert_ptr_eq(hash_find_channel("#a"), chptr);
  ck_assert_int_eq(dlink_list_length(&chptr->invites), 1);

  invite = find_invite(chptr, client2->client);

  ck_assert_ptr_ne(invite, NULL);
  ck_assert_ptr_eq(invite->client, client2->client);
  ck_assert_ptr_eq(invite->chptr, chptr);
  ck_assert_ptr_eq(invite->from, NULL);

  if (plexus_branch())
  {
    irc_join(client2, "#a");
    expect_message(client2, client2->client, "JOIN");

    // join removes invites
    ck_assert_int_eq(dlink_list_length(&chptr->invites), 0);
    ck_assert_int_eq(dlink_list_length(&client2->client->invited), 0);
  }
  else
  {
    // quitting removes invites
    exit_client(client2->client, &me, "forced quit");

    ck_assert_int_eq(dlink_list_length(&chptr->invites), 0);
  }
}

START_TEST(invite_remote)
{
  plexus_fork(other);
  plexus_up();

  struct MaskItem *conf = find_matching_name_conf(CONF_SERVER, "plexus4.2", NULL, NULL, 0);
  ck_assert_ptr_ne(conf, NULL);

  serv_connect(conf, NULL);

  WAIT_FOR(hash_find_client("plexus4.2") != NULL);
  ck_assert(IsServer(hash_find_client("plexus4.2")));

  struct PlexusClient *client = client_register("test1");

  WAIT_FOR(hash_find_client("test2") != NULL);
  struct Client *client2 = hash_find_client("test2");

  irc_join(client, "#a");
  expect_message(client, client->client, "JOIN");

  struct Channel *chptr = hash_find_channel("#a");
  ck_assert_ptr_ne(chptr, NULL);

  irc_invite(client, chptr, client2);
  expect_numeric(client, RPL_INVITING);

  ck_assert_ptr_ne(find_invite_from(chptr, client2, client->client), NULL);
  ck_assert_ptr_ne(find_invite_to(chptr, client2, client->client), NULL);

  exit_client(client2, &me, "forced quit"); // will desync other side

  ck_assert_int_eq(dlink_list_length(&chptr->invites), 0);
  ck_assert_int_eq(dlink_list_length(&client->client->localClient->invites), 0);
}
END_TEST

void
invite_setup(Suite *s)
{
  TCase *tc = tcase_create("invite");

  tcase_add_checked_fixture(tc, NULL, plexus_down);
  tcase_add_test(tc, invite);
  tcase_add_test(tc, invite_remote);

  suite_add_tcase(s, tc);
}
