/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2016 plexus development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "plexus_test.h"

static struct PlexusClient *
create(const char *name, struct irc_ssaddr *ip)
{
  int fds[2];

  if (socketpair(AF_UNIX, SOCK_STREAM, AF_UNSPEC, fds))
  {
    ck_abort_msg("socketpair failed");
    return NULL;
  }

  struct PlexusClient *pclient = MyMalloc(sizeof(struct PlexusClient));
  struct Listener *listener = MyMalloc(sizeof(struct Listener));
  struct Client *client = add_connection(listener, ip, fds[0]);

  pclient->client = client;
  strlcpy(pclient->name, name, sizeof(pclient->name));
  fd_open(&pclient->fd, fds[1], 1, "plexus client");

  comm_setselect(&pclient->fd, COMM_SELECT_READ, (PF *) io_read, pclient);

  return pclient;
}

struct PlexusClient *
client_create(const char *name)
{
  return client_create_ip(name, "127.0.0.1");
}

struct PlexusClient *
client_create_ip(const char *name, const char *ip)
{
  struct irc_ssaddr ss = ip_parse(ip);
  return create(name, &ss);
}

struct PlexusClient *
client_register(const char *name)
{
  struct PlexusClient *client = client_create(name);

  io_write(client, "USER %s . . :%s", client->name, client->name);
  io_write(client, "NICK %s", client->name);

  expect_numeric(client, RPL_WELCOME);

  return client;
}

struct PlexusClient *
client_register_host(const char *name, const char *ip)
{
  struct PlexusClient *client = client_create_ip(name, ip);

  io_write(client, "USER %s . . :%s", client->name, client->name);
  io_write(client, "NICK %s", client->name);

  expect_numeric(client, RPL_WELCOME);

  return client;
}
