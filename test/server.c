/*
 *  ircd-hybrid: an advanced Internet Relay Chat Daemon(ircd).
 *
 *  Copyright (C) 2016 Adam <Adam@anope.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include "plexus_test.h"

static struct PlexusClient *
create(const char *name, struct irc_ssaddr *ip)
{
  int fds[2];

  if (socketpair(AF_UNIX, SOCK_STREAM, AF_UNSPEC, fds))
  {
    ck_abort_msg("socketpair failed");
    return NULL;
  }

  struct PlexusClient *pclient = MyMalloc(sizeof(struct PlexusClient));
  struct Listener *listener = MyMalloc(sizeof(struct Listener));
  listener->flags = LISTENER_SERVER;
  struct Client *client = add_connection(listener, ip, fds[0]);

  pclient->client = client;
  strlcpy(pclient->name, name, sizeof(pclient->name));
  fd_open(&pclient->fd, fds[1], 1, "plexus server");

  comm_setselect(&pclient->fd, COMM_SELECT_READ, (PF *) io_read, pclient);

  return pclient;
}

struct PlexusClient *
server_create(const char *name)
{
  struct irc_ssaddr ss = ip_parse("127.0.0.1");
  return create(name, &ss);
}

struct PlexusClient *
server_register(const char *name)
{
  struct PlexusClient *client = server_create(name);

  io_write(client, "PASS password TS 6 02T");
  io_write(client, "SERVER %s 1 :test server", name);

  expect_message(client, NULL, "SERVER");

  return client;
}
