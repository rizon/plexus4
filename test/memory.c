/*
 *  ircd-hybrid: an advanced, lightweight Internet Relay Chat Daemon (ircd)
 *
 *  Copyright (c) 2016 plexus development team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 *  USA
 */

#include <stdlib.h>
#include <stddef.h>
#include <string.h>

/* alloc functions imported by libplexus for upgrade stuff */

void *ircd_alloc(size_t s);
void *ircd_realloc(void *s, size_t size);
void ircd_free(void *s);

struct chunk
{
  size_t sz;
  char memory[];
};

void *
ircd_alloc(size_t s)
{
  struct chunk *c = calloc(1, sizeof(struct chunk) + s);
  if (c == NULL)
    return NULL;

  c->sz = s;

  return c->memory;
}

void *
ircd_realloc(void *s, size_t size)
{
  if (!s)
    return ircd_alloc(size);

  if (!size)
  {
    ircd_free(s);
    return NULL;
  }

  struct chunk *c = (struct chunk *) (((char *) s) - sizeof(struct chunk));

  c = realloc(c, size);

  if (c == NULL)
    return NULL;

  c->sz = size;

  return c->memory;
}

void
ircd_free(void *s)
{
  struct chunk *c = (struct chunk *) (((char *) s) - sizeof(struct chunk));

  memset(c->memory, 0xaa, c->sz);
  free(c);
}
